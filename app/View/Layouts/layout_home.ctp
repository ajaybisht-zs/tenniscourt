<?php echo $this->Html->docType('html5'); ?>
<html lang="en">
    <head>
        <?php echo $this->Html->charset(); ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <?php
       
         echo $this->Html->meta('icon',
                   $this->Html->url('/img/favicon.png'));
        
        ?>
        <title><?php echo "BookAcourt"; ?></title>
        <?php
        echo $this->Html->css(
                array(
            'frontend/bootstrap.min',
            'frontend/style',
            'frontend/sticky-footer'
                ), null, array('inline' => false)
        );
        ?>
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,300italic,400italic,500italic,500,600,600italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'>
         <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300italic,300,400italic,600italic,600,700italic,700' rel='stylesheet' type='text/css'>
        <?= $this->fetch('css') ?>
    </head>
    <body class="Login_bg">
        <!-- Header Start -->
        <?php echo $this->element('frontend/header'); ?>
         <div class="top-settingss">
            <div class="top-white"></div>
        </div>
        
            <?php echo $this->Flash->render('positive');?>
            <?php echo $this->fetch('content'); ?>
       
        <?php echo $this->element('frontend/footer'); ?>

        <!-- Bootstrap core JavaScript
           ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <?php
        echo $this->Html->script(
                array(
                    'frontend/bootstrap.min',
                    'frontend/jquery.validate',
                )
        );
        echo $this->fetch('scriptBottom');
        ?>
        <script type="text/javascript">
            $(document).ready(function () {
            $('#media').carousel({
                pause: true,
                interval: 5200,
            });
        });
        </script>
    </body>
</html>