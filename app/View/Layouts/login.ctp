<!DOCTYPE html>
<html>
    <head>
      <title>Login</title>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <?php
          echo $this->Html->css(
              array(
                  'themify-icons',
                  'backend/login',
                  'backend/bootstrap-switch.min',
                  'bootstrap-datetimepicker.min'
              ),
              null, array('inline' => false)
          );
     ?>
      <?= $this->fetch('css') ?>
      <link href='https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
   </head>
   <body>
      <?php echo $this->fetch('content');?>
      <?php
        echo $this->Html->script(
               array(
                   'jQuery-2.1.4.min',
                   'bootstrap.min',
                   'jquery.validate',
                   'bootstrap-switch.min',
                   'users/login_reigster',
                   'moment.min',
                    'bootstrap-datetimepicker'
              
               )
           );
        echo $this->fetch('scriptBottom');
    ?>
   </body>
</html>
