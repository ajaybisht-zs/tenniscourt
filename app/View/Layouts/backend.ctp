<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Dashboard</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      
      <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
      <?php
          echo $this->Html->css(
              array(
                  'backend/AdminLTE',
                  'backend/skin-blue',
                  'backend/style',
                  'backend/custom-checkbox',
                  'backend/custom-font',
                  'frontend_dashboard/font-awesome',
                  'bootstrap-datetimepicker.min',
                  
                  
              ),
              null, array('inline' => false)
          );
        
     ?>

        <?= $this->fetch('css') ?>
   </head>
   <body class="hold-transition skin-blue sidebar-mini">
      <div class="wrapper">
         <?php echo $this->element('backend/header');?>
         
         <?php if($this->Session->read('Auth.User.user_role_id') == 2) { ?>
             <?php  echo $this->element('backend/sidebar');
            ?>
        
        <?php  } else {
            echo $this->element('backend/navigation'); 
        }  ?>
          
        
        
         <?php echo $this->fetch('content');?>
          <?php echo $this->Html->script(
            array(
                'jQuery-2.1.4.min',
                'jquery.validate',
                'bootstrap.min',
                'moment.min',
                'bootstrap-datetimepicker',
                'underscore-min',
            )

        ); ?>
      </div>
      <?php 
        echo $this->element('User/change_password');
          
       ?>
      <div id="edit-profile" class="modal fade" role="dialog">
          <div class="modal-dialog">
              <!-- Modal content-->
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Edit Profile</h4>
                  </div>
                  <div class="modal-body staff-view clearfix" id="view-user">
                  </div>
              </div>

          </div>
      </div>      
      <?= $this->fetch('script') ?>
      <?= $this->fetch('scriptBottom') ?>
      
  </body>
</html>
