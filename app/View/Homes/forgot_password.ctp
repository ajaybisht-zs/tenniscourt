<div class="col-md-6 col-lg-offset-3 col-sm-offset-2 col-md-offset-3 login-inner-wraper padding-zero">
    <div class="inner-login">
        <?php echo $this->Form->create(
              'User',
              [
                'class' => 'login-form',
                'novalidate' => true,
                'type'=>'post',
                'url' => [
                  'controller' =>'Homes',
                  'action' => 'forgotPassword',
                ],
                'id' => 'forgotpassword-form'
              ]
            );
        ?>
        <h2 class="text-center">Forget Password</h2>
            <p class="text-center">Enter your E-Mail address and we will send you a reset password link</p>
            <div class="form-group">
                <?php echo $this->Form->input('email',
                    [
                      'class' => 'form-control',
                      'placeholder' => __('Email'),
                      'label' => false
                    ]
                  );
                ?>
            </div>
        
            
            <div class="form-group text-center">
                <?php echo $this->Form->button(
                        __('Request'),
                        [
                          'type' => 'submit',
                          'class' => 'btn btn-green btn-block'
                        ]
                      );
                ?>
            </div>

            <div class="col-lg-12">               
                <?php echo $this->Html->link(
                        __('Back to Log in'),
                        ['controller'=>'Homes','action'=>'login'],
                        [
                          'type' => 'button',
                          'class' => 'btn btn-block'
                        ]
                      );
                ?>
              </div>
        </form>
    </div>
</div>
<?php echo $this->Html->script('users/login_reigster',array('block' => 'scriptBottom'));?>