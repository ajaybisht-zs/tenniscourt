<div class="col-md-6 col-lg-offset-3 col-sm-offset-2 col-md-offset-3 login-inner-wraper padding-zero">
    <div class="inner-login">
        <?php echo $this->Form->create('User',array('url' => array('controller' => 'Homes','action' => 'login'),'class'=>'login-form','id' => 'loginform')); ?>
            <h2 class="text-center">Login Now</h2>
            <div class="form-group">
                <?php echo $this->Form->input('User.email',
                          [
                            "class"=>"form-control",
                            "label" => false,
                            'div' > false,
                            'id' => 'exampleInputEmail1',
                            'Placeholder' => __('Email'),
                            "required"
                          ]
                      )
                ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->input('User.password',
                      [
                        "class"=>"form-control",
                        "label" => false,
                        'div' => false,
                        'id' => 'exampleInputPassword1',
                        'placeholder' => __('Password'),
                        "required"
                      ]
                  )
                ?>
            </div>
            <div class="form-group clearfix">
                <div class="col-lg-6 col-xs-12 padding-left">
                    <div class="checkbox">
                        <label>
                        <input type="checkbox"> remember me
                        </label>
                    </div>
                </div>
                <div class="col-lg-6 col-xs-12 padding-right text-right forget-pass">
                    <?php
                        echo $this->Html->link(
                            'Forgot Password',
                            array('controller' => 'Homes', 'action' => 'forgotPassword'),
                            array(
                                'escape' => false,
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-green btn-block">Log in</button>
            </div>
            <!--div class="form-group clearfix">
                <div class="col-sm-12 padding-l-r ">
                    <div class="border-top">
                        <div class="or-text">Join with</div>
                    </div>
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-lg-6 padding-left">
                    <button class="btn btn-fb btn-block">FACEBOOK</button> 
                </div>
                <div class="col-lg-6 padding-right">
                    <button class="btn  btn-block btn-twiiter">TWITTER</button> 
                </div>
            </div-->
        </form>
    </div>
</div>
<?php echo $this->Html->script('users/login_reigster',array('block' => 'scriptBottom'));?>