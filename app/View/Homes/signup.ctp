<div class="col-md-6 col-lg-offset-3 col-sm-offset-2 col-md-offset-3 login-inner-wraper register padding-zero">
    <div class="inner-login">
         <?php echo $this->Form->create('User',array('url' => array('controller' => 'Homes','action' => 'signup'),'class'=>'login-form','id' => 'signupform')); ?>
            <h2 class="text-center">Register Now</h2>
            <div class="form-group">
                <?php
                echo $this->Form->input('first_name', array('label' => false, 'div' => false, 'placeholder' => 'First Name', 'class' => 'form-control'));
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->input('last_name', array('label' => false, 'div' => false, 'placeholder' => 'Last Name', 'class' => 'form-control'));
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->input('email', array('type' => 'email','label' => false, 'div' => false, 'placeholder' => 'E-mail', 'class' => 'form-control'));
                ?>
            </div>
            <div class="form-group">
                <?php
                echo $this->Form->input('UserProfile.phone', array('label' => false, 'div' => false, 'placeholder' => 'Phone Number', 'class' => 'form-control'));
                ?>
            </div>
             <div class="form-group">
                <?php
                echo $this->Form->input('UserProfile.country', array('label' => false, 'div' => false, 'placeholder' => 'Country', 'class' => 'form-control'));
                ?>
            </div>

             <div class="form-group clearfix">
             <div class="col-lg-6 padding-left">
                <?php
                echo $this->Form->input('UserProfile.state', array('label' => false, 'div' => false, 'placeholder' => 'State', 'class' => 'form-control'));
                ?>
             </div>
             <div class="col-lg-6 padding-right">
                <?php
                echo $this->Form->input('UserProfile.zipcode', array('label' => false, 'div' => false, 'placeholder' => 'Zipcode', 'class' => 'form-control'));
                ?>
                </div>
            </div>

             <div class="form-group">
                <?php
                echo $this->Form->input('password', array('label' => false, 'div' => false, 'placeholder' => 'Password', 'id'=>'conf-pass','class' => 'form-control'));
                ?>
            </div>
              <div class="form-group">
               <?php
                echo $this->Form->input('confirm_password', array('label' => false, 'div' => false, 'placeholder' => 'Confirm Password','type' => 'password', 'class' => 'form-control'));
                ?>
            </div>

           <div class="form-group clearfix">
                <label class="col-lg-12 padding-left">Type of User</label>
                <div class="col-lg-12 padding-left">
                    <label class="radio-inline"><input type="radio" value= 3 name="User[user_role_id]" checked>User</label>
                    <label class="radio-inline"><input type="radio" value= 2 name="User[user_role_id]">Court Owner</label>
                </div>
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-green btn-block">Register</button>
            </div>
            <!--div class="form-group clearfix">
                <div class="col-sm-12 padding-l-r ">
                    <div class="border-top">
                        <div class="or-text">Join with</div>
                    </div>
                </div>
            </div-->
            <!--div class="form-group clearfix">
                <div class="col-lg-6 padding-left">
                    <button class="btn btn-fb btn-block">FACEBOOK</button> 
                </div>
                <div class="col-lg-6 padding-right">
                    <button class="btn  btn-block btn-twiiter">TWITTER</button> 
                </div>
            </div-->
        </form>
    </div>
</div>
<?php echo $this->Html->script('users/login_reigster',array('block' => 'scriptBottom'));?>