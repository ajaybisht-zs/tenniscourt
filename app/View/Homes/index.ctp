<div id="myCarousel" class="carousel carousel-custom slide carousel-fade" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
            </ol>
            <div class="container">
                <div class="text-section">
                    <div class="border-thick">
                        <div class="main-heading-banner">
                            <h1>Free Online Booking and Scheduling for <span class="green-text"> Tennis Courts </span></h1>
                            <p>urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>
                            <button class="btn btn-white">Find My Nearest Venues</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <?php echo $this->Html->image('home-banner-bg.png',array('alt' => 'slider'));?>
                </div>
            </div>

        </div>

     
        <!-- Find Section
            ================================================== 
                                                               -->
        <div class="find-section">
            <div class="container">
                <h3 class="text-center">Find Where You Want to Play</h3>
                <form class="form clearfix">
                    <div class="col-lg-12">
                        <div class="col-lg-4 col-sm-4">
                            <div class="form-group">
                                <label>Tennis Courts Near:</label>
                                <input type="text" class="form-control"  placeholder="City Name/Zip code or State">
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-3">
                            <div class="form-group">
                                <label>Number of Courts:</label>
                                <select class="form-control">
                                    <option>Select Court</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-3">
                            <div class="form-group">
                                <label>Date</label>
                                <div class='input-group date' id='datetimepicker2'>
                                    <input type='text' class="form-control" />
                                    <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-2">
                            <div class="form-group">
                                <button type="submit" class="btn btn-find">Find Now</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- Easy Proces
            ================================================== 
                                                               -->
        <div class="easy-process">
            <div class="container ">
                <h3 class="text-center"> Easy Process </h3>
                <div class="col-lg-4 col-sm-4 seprator">
                    <?php  echo $this->Html->image('find-court.png',array('class' =>'img-responsive','alt' => 'find'));?>
                    <h4 class="text-center">
                        Find Tennis Court
                    </h4>
                </div>
                <div class="col-lg-4 col-sm-4 seprator">
                    <?php  echo $this->Html->image('schedule.png',array('class' =>'img-responsive','alt' => 'find'));?>
                    <h4 class="text-center">
                        Book A Court 
                    </h4>
                </div>
                <div class="col-lg-4 col-sm-4">
                    <?php  echo $this->Html->image('enjoy.png',array('class' =>'img-responsive enjoy-game','alt' => 'find'));?>
                    <h4 class="text-center">
                        Enjoy Game 
                    </h4>
                </div>
            </div>
        </div>


        <!-- Recommended Court Near to you
            ================================================== 
                                                               -->
        <div class="court-near">
            <div class="container">
                <h3 class="text-center">Recommended Court Near You</h3>
                <div class="col-lg-4 col-sm-4 bg-white">
                    <div class="inner-court-box">
                        <?php  echo $this->Html->image('court-img.png',array('class' =>'img-responsive','alt' => 'img'));?>
                        <h4><span class="title-mahroon">Queensland </span> <span class="green-texts">Tennis Centre </span></h4>
                        <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum,</p>
                        <div class="text-center">
                            <button class="btn btn-round ">$30/hr Book Now</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-4 bg-white">
                    <div class="inner-court-box">
                        <?php  echo $this->Html->image('img1.png',array('class' =>'img-responsive','alt' => 'img'));?>
                        <h4><span class="title-mahroon">Australian </span>  <span class="green-texts">Centre </span> </h4>
                        <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum,</p>
                        <div class="text-center">
                            <button class="btn btn-round ">$30/hr Book Now</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-4 bg-white">
                    <div class="inner-court-box">
                        <?php  echo $this->Html->image('img2.png',array('class' =>'img-responsive','alt' => 'img'));?>
                        <h4><span class="title-mahroon">Queensland </span> <span class="green-texts">Tennis Centre </span> </h4>
                        <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum,</p>
                        <div class="text-center">
                            <button class="btn btn-round ">$30/hr Book Now</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

 <!-- Recommended Court Near to you
            ================================================== 
                                                               -->


       <div class="fatured-partners"> 
       <div class="container">

       <h3 class="text-center">Our Featured Partners</h3>

    <div class="carousel slide media-carousel" id="media">
        <div class="carousel-inner">
          <div class="item  active">
            <div class="row">
              <div class="col-lg-2 col-sm-2">
                <?php
                    echo $this->Html->link(
                        $this->Html->image('tennis.png',array('class' =>'img-responsive','alt' => 'logo')),
                        '#',
                        [
                            'escape' => false,
                            'class' => 'thumbnail'
                        ]
                    );
                ?>
              </div>          
              <div class="col-lg-2 col-sm-2">
                <?php
                    echo $this->Html->link(
                        $this->Html->image('junior-tour.png',array('class' =>'img-responsive','alt' => 'logo')),
                        '#',
                        [
                            'escape' => false,
                            'class' => 'thumbnail'
                        ]
                    );
                ?>
              </div>
              <div class="col-lg-2 col-sm-2">
                <?php
                    echo $this->Html->link(
                        $this->Html->image('hot-shots.png',array('class' =>'img-responsive','alt' => 'logo')),
                        '#',
                        [
                            'escape' => false,
                            'class' => 'thumbnail'
                        ]
                    );
                ?>
              </div>
               <div class="col-lg-2 col-sm-2">
                <?php
                    echo $this->Html->link(
                        $this->Html->image('kooy.png',array('class' =>'img-responsive','alt' => 'logo')),
                        '#',
                        [
                            'escape' => false,
                            'class' => 'thumbnail'
                        ]
                    );
                ?>
              </div> 
               <div class="col-lg-2 col-sm-2">
                <?php
                    echo $this->Html->link(
                        $this->Html->image('cardio.png',array('class' =>'img-responsive','alt' => 'logo')),
                        '#',
                        [
                            'escape' => false,
                            'class' => 'thumbnail'
                        ]
                    );
                ?>
              </div>    
                     <div class="col-lg-2 col-sm-2">
                <?php
                    echo $this->Html->link(
                        $this->Html->image('hot-shots.png',array('class' =>'img-responsive','alt' => 'logo')),
                        '#',
                        [
                            'escape' => false,
                            'class' => 'thumbnail'
                        ]
                    );
                ?>
              </div>      
            </div>
          </div>
            <div class="item ">
            <div class="row">
              <div class="col-lg-2 col-sm-2">
                <?php
                    echo $this->Html->link(
                        $this->Html->image('tennis.png',array('class' =>'img-responsive','alt' => 'logo')),
                        '#',
                        [
                            'escape' => false,
                            'class' => 'thumbnail'
                        ]
                    );
                ?>
              </div>          
              <div class="col-lg-2 col-sm-2">
                <?php
                    echo $this->Html->link(
                        $this->Html->image('junior-tour.png',array('class' =>'img-responsive','alt' => 'logo')),
                        '#',
                        [
                            'escape' => false,
                            'class' => 'thumbnail'
                        ]
                    );
                ?>
              </div>
              <div class="col-lg-2 col-sm-2">
                <?php
                    echo $this->Html->link(
                        $this->Html->image('hot-shots.png',array('class' =>'img-responsive','alt' => 'logo')),
                        '#',
                        [
                            'escape' => false,
                            'class' => 'thumbnail'
                        ]
                    );
                ?>
              </div>
               <div class="col-lg-2 col-sm-2">
                <?php
                    echo $this->Html->link(
                        $this->Html->image('kooy.png',array('class' =>'img-responsive','alt' => 'logo')),
                        '#',
                        [
                            'escape' => false,
                            'class' => 'thumbnail'
                        ]
                    );
                ?>
              </div> 
               <div class="col-lg-2 col-sm-2">
                <?php
                    echo $this->Html->link(
                        $this->Html->image('cardio.png',array('class' =>'img-responsive','alt' => 'logo')),
                        '#',
                        [
                            'escape' => false,
                            'class' => 'thumbnail'
                        ]
                    );
                ?>
              </div>    
                     <div class="col-lg-2 col-sm-2">
                <?php
                    echo $this->Html->link(
                        $this->Html->image('hot-shots.png',array('class' =>'img-responsive','alt' => 'logo')),
                        '#',
                        [
                            'escape' => false,
                            'class' => 'thumbnail'
                        ]
                    );
                ?>
              </div>      
            </div>
          </div>
    
        </div>
        <a data-slide="prev" href="#media" class="left carousel-control"></a>
        <a data-slide="next" href="#media" class="right carousel-control"></a>
      </div> 
       </div>
       
       </div>                                     