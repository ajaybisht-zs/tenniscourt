<div class="content-wrapper ">
<?php echo $this->Flash->render('positive');?>
<section class="content-header col-xs-12 clearfix another-page">
    <div class="col-lg-6 col-xs-6 ">
        <h1 class="heading-text-color">My Courts</h1>
    </div>
    <div class="col-lg-6 col-xs-6 left-page-button">
        <button class="btn btn-mahroon   pull-right" data-toggle="modal" data-target="#addCourt"><i class="fa fa-plus" aria-hidden="true"></i> Add New Court</button>
    </div>
</section>
<section class="content dashboard-box clearfix col-xs-12 ">
    <div class="table-responsive">
        <table class="table shoping-cart-table table-striped">
            <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('court_name', 'Court Name');?></th>
                     <th><?php echo $this->Paginator->sort('court_number', 'Court Number');?></th>
                    <th><?php echo $this->Paginator->sort('court_type', 'Court type');?></th>
                    <th><?php echo $this->Paginator->sort('surface_id', 'Court Surface');?></th>
                    <th><?php echo $this->Paginator->sort('', 'Total Bookings');?></th>
                    <th><?php echo $this->Paginator->sort('hourly_rate', 'Price');?></th>
                    <th><?php echo $this->Paginator->sort('is_active', 'Status');?></th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            <?php
                    if(!empty($courts)) { 
                        foreach ($courts as $court) {
                            //pr($court);die;
                ?>
                <tr>
                    <td>
                        <?php echo $court['Court']['court_name']; ?>
                    </td>
                    <td>
                        <?php echo $court['Court']['court_number']; ?>
                    </td>
                    <td><?php echo ($court['Court']['court_type'] == 0) ? "Indoor" : "Outdoor"; ?></td>
                    <td><?php echo $court['Surface']['surface_name']; ?></td>
                    <td>250</td>
                    <td>$<?php echo $court['Court']['hourly_rate']; ?>/hr
                    </td>
                        <td>
                            <?php if($court['Court']['is_active'] == 0) {?>
                                <span class="text-warning">Pending</span>
                            <?php } elseif ($court['Court']['is_active'] == 1) { ?>
                                <span class="text-success">Approve</span>
                              <?php   } else { ?>
                                <span class="text-danger">Disapprove</span>
                              <?php  } ?>
                        </td>
                        <td>
                            <a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a>
                            <a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                
                <?php } } else { ?>
                    <tr>
                        <td colspan="5">
                            No Record Found
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php echo $this->element('backend/pagination'); ?>
    </section>
</div>
<?php echo $this->element('homes/add_court');?>