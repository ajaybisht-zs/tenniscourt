<div class="col-md-6 col-lg-offset-3 col-sm-offset-2 col-md-offset-3 login-inner-wraper padding-zero">
    <div class="inner-login">
        <?php echo $this->Form->create(
              'User',
              [
                'class' => 'login-form',
                'novalidate' => true,
                'type' => 'post',
                'url' => [
                 'controller' => 'Homes',
                  'action' => 'recoverPassword',
                ],
                'id' => 'recoverpassword-form'
              ]
            );
        ?>
        <h2 class="text-center">Reset Password</h2>
            <div class="form-group">
                <?php echo $this->Form->hidden('token',
                        [
                          'value' => $this->request['pass'][0]
                        ]
                      );
                  ?>
                  <?php echo $this->Form->input('password',
                        [
                          'class' => 'form-control',
                          'placeholder' => __('New Passoword'),
                          'label' => false,
                          'id' => 'recoverconfirm'
                        ]
                      );
                  ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->input('new_password',
                    [
                      'type' => 'password',
                      'class' => 'form-control',
                      'placeholder' => __('Confirm Passoword'),
                      'label' => false
                    ]
                  );
                ?>
            </div>
      
            
            <div class="form-group text-center">
                <?php echo $this->Form->button(
                    __('Submit'),
                    [
                      'type' => 'submit',
                      'class' => 'btn btn-green btn-block'
                    ]
                  );
                ?>
            </div>
          
        </form>
    </div>
</div>
<?php echo $this->Html->script('users/login_reigster',array('block' => 'scriptBottom'));?>