<div class="site-wrapper">
  <div class="site-wrapper-inner">
    <div class="login-layout-wraper">
    <?= $this->Flash->render('positive') ?>
        <div class="col-center col-lg-5 signup-section clearfix">
          <div class="left-section col-lg-5">
            <div class="text-center logo-img">
                <?php echo $this->Html->image('logo.png'); ?> 
            </div>
         </div>
        <div class="right-section col-lg-7">
          <div class="create-account">
            <h2 class="text-center"><?= __('Welcome Let\'s get started'); ?></h2>
                <div class="login">
                    <h2 class="text-left login-heading"><?= __('Administration Login'); ?></h2>
                    <?= $this->Form->create(
                          '',
                          [ 'url' => array('controller' => 'users', 'action' => 'login'),
                            'type' => 'post',
                            'class' => 'form-horizontal signup-form',
                            'novalidate' => true,
                            'id' => 'login-form',
                            'name' => 'loginform'
                          ]
                        );
                    ?>
                      <div class="form-group">
                         <div class="col-lg-12">
                            <div class="input-group clearfix">
                              <div class="input-group-addon "><i class="glyphicon glyphicon-user"></i></div>
                                <?= $this->Form->input('User.email',
                                      [
                                        "class"=>"form-control",
                                        "label" => "",
                                        'Placeholder' => __('Enter your username'),
                                        "required"
                                      ]
                                  )
                                ?>
                            </div>
                         </div>
                      </div>
                      <div class="form-group">
                         <div class="col-lg-12">
                            <div class="input-group">
                               <div class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></div>
                               <?= $this->Form->input('User.password',
                                      [
                                        "class"=>"form-control",
                                        "label" => "",
                                        'placeholder' => __('Enter your password'),
                                        "required"
                                      ]
                                  )
                                ?>
                            </div>
                         </div>
                      </div>
                      <div class="form-group">
                         <div class="col-lg-12"> 
                            <?=
                              $this->Form->button(
                                  __('Log In'),
                                  [
                                    'type' => 'submit',
                                    'class' => 'btn btn-border-white btn-block'
                                  ]
                                );
                            ?>
                         </div>
                      </div>
                    <?= $this->Form->end(); ?>
                   
                   </div>
                   
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
