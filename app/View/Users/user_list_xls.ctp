<?php
header("Content-type:application/vnd.ms-excel");
header("Content-disposition:attachment;filename=UserSheetreport.xls");
?>
<table id="table" class="table table-hover table-striped">
            <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('first_name', 'First Name');?></th>
                    <th><?php echo $this->Paginator->sort('last_name', 'Last Name');?></th>
                    <th><?php echo $this->Paginator->sort('email', 'Email');?></th>
                    <th><?php echo $this->Paginator->sort('phone', 'Phone');?></th>
                    <th><?php echo $this->Paginator->sort('created', 'Created');?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(!empty($users)) { 
                        foreach ($users as $user) {
                ?>
                            <tr>
                                <td>
                                    <?php echo $user['User']['first_name']; ?>
                                </td>
                                <td>
                                    <?php echo $user['User']['last_name']; ?>
                                </td>
                                <td>
                                    <?php echo $user['User']['email']; ?>
                                </td>
                                 <td>
                                    <?php echo $user['User']['phone']; ?>
                                </td>
                                 <td>
                                    <?php echo $user['User']['created']; ?>
                                </td>
                            </tr>
                <?php } } else { ?>
                    <tr>
                        <td colspan="5">
                            No Record Found
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
 </table>