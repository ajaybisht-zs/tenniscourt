<div class="content-wrapper">
<?php echo $this->Flash->render('positive') ?>
    <section class="content-header clearfix">
        <div class="col-lg-6">
            <h1 class="heading-text-color">Club Admin Listing</h1>
        </div>
        <div class="col-lg-6 text-right">
            <?php
                echo $this->Html->link(
                    '<i class="ti-plus"></i><span>' . __('Add Club Admin') . '</span>',
                    array('controller' => 'Users', 'action' => 'add'),
                    array(
                        'escape' => false,
                        'class'=> 'btn btn-green'
                    )
                );
            ?>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="header-section ">

                <div class="search-bar-panel">
               
                <?php
                    echo $this->Form->Create(null, array(
                            'url' => ['controller' => 'Users', 'action' => 'index'],
                            'type' => 'get',
                            'class'=>'form-inline form-inline-setting staf-search',
                        )
                    );
                ?>
                <div class="form-group">
                    <?php
                        echo $this->Form->input('search', array(
                               'class' => 'form-control',
                               'placeholder' =>"Search by name or email address",
                               'label'=>FALSE,
                               'value' => (isset($_GET['search']) && !empty($_GET['search'])) ? $_GET['search'] : ''
                            )
                        );
                    ?>
                    </div>
                <button type="submit" class="btn btn-green">Search</button>
                    <?php echo $this->Form->end();?>

                    </div>

                    <div id="deleteRecords" class="btn-group delete-staff" style="display:none;">
                        <button class="btn btn-default btn-sm"><i class="ti-trash"></i></button>
                    </div>
                </div>
             <table id="table" class="table table-hover table-striped">
            <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('first_name', 'First Name');?></th>
                    <th><?php echo $this->Paginator->sort('last_name', 'Last Name');?></th>
                    <th><?php echo $this->Paginator->sort('email', 'Email');?></th>
                    <th><?php echo __('Actions'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(!empty($users)) { 
                        foreach ($users as $user) {
                ?>
                            <tr>
                                <td>
                                    <?php echo $user['User']['first_name']; ?>
                                </td>
                                <td>
                                    <?php echo $user['User']['last_name']; ?>
                                </td>
                                <td>
                                    <?php echo $user['User']['email']; ?>
                                </td>
                               
                                <td id="inline-popups">
                                    <?php echo $this->Html->link(
                                                    '<i class="glyphicon glyphicon-pencil" aria-hidden="true"></i>',
                                                    'javascript:void(0)',
                                                    array(                                           
                                                            'class' => 'editEmployee btn',
                                                            'data-url' => $this->Html->url(
                                                             [      
                                                                 'controller' => 'Users',                                                
                                                                 "action" => "editEmployee",
                                                                 base64_encode($user['User']['id'])
                                                             ],true
                                                         ),
                                                            'escape' => false,
                                                            'title' => 'edit'
                                                        )
                                        );?>
                                        <?php echo $this->Html->link(
                                                    '<i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i>',
                                                    'javascript:void(0)',
                                                    array(                                           
                                                            'class' => 'viewEmployee btn',
                                                            'data-url' => $this->Html->url(
                                                             [      
                                                                 'controller' => 'Users',                                                
                                                                 "action" => "viewEmployee",
                                                                 base64_encode($user['User']['id'])
                                                             ],true
                                                         ),
                                                            'escape' => false,
                                                            'title' => 'view'
                                                        )
                                        );?>
                                </td>
                            </tr>
                <?php } } else { ?>
                    <tr>
                        <td colspan="5">
                            No Record Found
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php echo $this->element('backend/pagination'); ?>
        </div>
    </section>
    <!-- /.content -->
</div>
<div id="edit-client" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Client</h4>
            </div>
            <div class="modal-body staff-view clearfix" id="view-user">
          
            </div>
       
        </div>

    </div>
</div>
<div id="view-client" class="modal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Client Details</h4>
            </div>
            <div class="modal-body staff-view clearfix" id="view">
          
            </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>

    </div>
</div>
<?php
echo $this->Html->script('users/view',array('block' => 'scriptBottom'));
?>