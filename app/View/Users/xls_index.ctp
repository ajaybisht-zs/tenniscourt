<div class="content-wrapper">
<?php echo $this->Flash->render('positive') ?>
    <section class="content-header clearfix">
        <div class="col-lg-6">
            <h1 class="heading-text-color">XLS Listing</h1>
        </div>
        
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="header-section ">

               
                    <div id="deleteRecords" class="btn-group delete-staff" style="display:none;">
                        <button class="btn btn-default btn-sm"><i class="ti-trash"></i></button>
                    </div>
                </div>
             <table id="table" class="table table-hover table-striped">
            <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('', 'Filename');?></th>
                    
                    <th><?php echo __('Actions'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php //pr($files);die;
                    if(!empty($files)) { 
                        foreach ($files as $file) {        
                ?>
                            <tr>
                                <td>
                                    <?php echo $file['File']['filename']; ?>
                                </td>
                               
                                <td id="inline-popups">
                                    <?php
                                            echo ($file['File']['status'] == 1) ? "saved to dropbox" : $this->Html->link(
                                            'save to dropbox',
                                            array('controller' => 'Users','action' => 'saveDropbox',$file['File']['id']),
                                            array(
                                                   
                                                'escape' => false  ,
                                                'class'=>'viewuserlist btn btn-primary',

                                                
                                                
                                                )
                                        );
                                    ?>
                                    <?php echo ($file['File']['status'] == 2) ? "saved to Quickbook" : $this->Html->link(
                                                    'import to quickbook',
                                                    array('controller' => 'Users','action' => 'saveQuickbook',$file['File']['id']),
                                                    array(                                           
                                                            'class' => 'editEmployee btn btn-primary',
                                                            
                                                        
                                                            'escape' => false,
                                                            
                                                        )
                                        );?>
                                </td>
                            </tr>
                <?php  }   } else { ?>
                    <tr>
                        <td colspan="5">
                            No Record Found
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php //echo $this->element('backend/pagination'); ?>
        </div>
    </section>
    <!-- /.content -->
</div>
