<!-- app/View/Users/add.ctp -->
<div class="content-wrapper">
<?= $this->Flash->render('positive') ?>
<section class="content-header clearfix">
        <div class="col-lg-6">
            <h1 class="heading-text-color">Manage Clients</h1>
            <div class="text-gry">
                <ol class="breadcrumb " style="margin-bottom: 5px;">
                    <li><?php
                    echo $this->Html->link(
                        '</i><span class="breadcrumb">' . __('Clients') . '</span>',
                        array('controller' => 'Clients', 'action' => 'index'),
                        array(
                            'escape' => false
                        )
                    );
                ?>  
                </li>
                    <li class="text-gry"><?= __('Add Client'); ?></li>
                </ol>
            </div>
        </div>
</section>
<section class="content">
    <div class="box clearfix">
    <div class="col-sm-6 col-lg-4 col-lg-offset-4 col-sm-offset-3">
    <?php echo $this->Form->create('User', array('id' => 'admin-add-user')); ?>
    <div class="form-group right-inner-addon">
        <?php
        echo $this->Form->input('first_name', array('label' => false, 'div' => false, 'placeholder' => 'First Name', 'class' => 'form-control'));
        ?>
    </div>
    <div class="form-group right-inner-addon">
        <?php
        echo $this->Form->input('last_name', array('label' => false, 'div' => false, 'placeholder' => 'Last Name', 'class' => 'form-control'));
        ?>
    </div>
    <div class="form-group right-inner-addon">
        <?php
        echo $this->Form->input('email', array('label' => false, 'div' => false, 'placeholder' => 'email', 'class' => 'form-control'));
        ?>

    </div>
    
    <div class="form-group right-inner-addon">
        <?php
        echo $this->Form->input('password', array('label' => false, 'div' => false, 'placeholder' => 'Password here', 'class' => 'form-control'));
        ?>
    </div>
    <div class="form-group right-inner-addon">
        <?php
        echo $this->Form->input('password_confirm', array('label' => false,'type' =>'password' ,'div' => false, 'placeholder' => 'Re-enter Password here', 'class' => 'form-control'));
        ?>
    </div>
    
    <div class="form-group btn-margin-bottom right-inner-addon">
        <button class="btn btn-black btn-block">Add Client</button>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
</div>
</section>
</div>
<?php echo $this->Html->script('users/subadmin',array('block' => 'scriptBottom'));?>