<div class="content-wrapper">
<?= $this->Flash->render('positive') ?>
    <section class="content-header clearfix">
        <div class="col-lg-6">
            <h1 class="heading-text-color">Email Template Listing</h1>
        </div>
    </section>
        <section class="content">

               <div class="box box-success">
                  <div class="box-header with-border">
                    <h3 class="box-title">Manage Email Templates</h3>

                    <div class="box-tools pull-right">
                      <div class="has-feedback">
                      <?php
                    echo $this->Form->create('User', array(
                                    'url' => '/admin/email_templates/',
                                    'type' => 'get',
                                    'novalidate' => true
                                    ));
                ?>
        <?php
            echo $this->Form->input(
                                    'search',
                                    array(
                                        'div' => false,
                                        'label' => false,
                                        'class' => 'form-control input-sm',
                                        'placeholder' => 'search',
                                        'escape' => false,
                                        'default' => (isset($this->request->query['search']) && !empty($this->request->query['search'])) ? $this->request->query['search'] : ''
                                        )
                                );
                            ?>
                        <span class="form-control-feedback">
                           <?php
                                    echo $this->Form->button(
                                        '<i class="glyphicon glyphicon-search"></i>',
                                        array(
                                            'type' => 'submit',
                                            'escape' => false
                                            )
                                    );
                                ?> 
                        </span>
                      </div>
                      <?php echo $this->Form->end(); ?>
                    </div>
                    <!-- /.box-tools -->
                  </div>
                



<!-- /.box-header -->
                  <div class="box-body no-padding">
                    <div class="table-responsive mailbox-messages">
            <table id="table" class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>
                            <?php echo $this->Paginator->sort('EmailTemplate.template_used_for', 'Template Used For', array('class' => 'tableinherit tabl-a')); ?>
                        </th>
                        <th>
                            <?php echo $this->Paginator->sort('EmailTemplate.subject', 'Subject', array('class' => 'tableinherit tabl-a')); ?>
                        </th>
                        <th>
                            <?php echo $this->Paginator->sort('EmailTemplate.modified', 'Last Modified', array('class' => 'tableinherit tabl-a')); ?>
                        </th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if(!empty($emailTemplates)) { 
                            foreach ($emailTemplates as $showEmailTemplates) {
                    ?>
                                <tr>
                                    <td>
                                        <?php echo $showEmailTemplates['EmailTemplate']['template_used_for']; ?>
                                    </td>
                                    <td>
                                        <?php echo $showEmailTemplates['EmailTemplate']['subject']; ?>
                                    </td>
                                    <td>
                                        <?php echo date('d M, Y', strtotime($showEmailTemplates['EmailTemplate']['modified'])); ?>
                                    </td>
                                    <td>
                                        <?php
                                            echo $this->Html->link(
                                                    '<i class="glyphicon glyphicon-edit"></i>',
                                                    '/admin/email_templates/editTemplate/'.base64_encode($showEmailTemplates['EmailTemplate']['id']),
                                                    array(

                                                            'class' => 'btn btn-success btn-sm', 
                                                            'escape' => false,
                                                            'title' =>'edit'
                                                        )
                                                );
                                        ?>
                                    </td>
                                </tr>
                    <?php } } else { ?>
                        <tr>
                            <td colspan="6">
                                No Record Found
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php echo $this->element('backend/pagination'); ?>

 </div>
</section>
</div>
<style type="text/css">
    button{
        border: transparent;
    background: transparent;
    }
    
</style>
<script>
     var $rows = $('#table tr');
$('#UserSearch').keyup(function() {
    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
    
    $rows.show().filter(function() {
        var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
        return !~text.indexOf(val);
    }).hide();
});

</script>