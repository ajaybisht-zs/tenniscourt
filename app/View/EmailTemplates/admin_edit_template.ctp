<?php
	echo $this->Html->script('fckeditor');
	echo $this->Html->script('backend/Admin/edit_template', array('inline' => false));
?>
<div class="content-wrapper">
<?= $this->Flash->render('positive') ?>
    <section class="content-header clearfix">
        <div class="col-lg-6">
            <h1 class="heading-text-color">Manage Email Template</h1>
            <div class="text-gry">
                <ol class="breadcrumb " style="margin-bottom: 5px;">
                    <li><?php
                    echo $this->Html->link(
                        '</i><span class="breadcrumb">' . __('Email Template') . '</span>',
                        array('controller' => 'EmailTemplates', 'action' => 'index','admin' =>true),
                        array(
                            'escape' => false
                        )
                    );
                ?> 	
                </li>
                    <li class="text-gry"><?= __('Edit Email Template'); ?></li>
                </ol>
            </div>
        </div>
    </section>
<section class="content">
               <div class="row">
                  <!-- left column -->

                  <div class="col-md-12">
                     <!-- Client Information -->
                     <div class="box box-success">
                        <div class="box-header with-border">
                        <h3 class="box-title">Email Template</h3>
                        </div>
	    	<?php
	    		echo $this->Form->create(
	    				'EmailTemplate',
	    				array(
	    					'url' => '/admin/email_templates/editTemplate',
	    					'method' => 'post',
	    					'class' => 'form-horizontal',
	    					'id' => 'EditEmailTemplate',
	    					'admin' => true
	    					)
	    			);
	    			echo $this->Form->hidden('id', array('value' => $template['EmailTemplate']['id']));
	    	?>
	            <div class="box-body">
	                    <div class="form-group">
	                    <label>Template Used For<span class="require">*</span></label>
	                    	<?php
	                    		echo $this->Form->input('template_used_for', array(
	                    			'div' => false,
	                    			'label' => false,
	                    			'class' => 'form-control',
	                    			'default' => $template['EmailTemplate']['template_used_for']
	                    			)
	                    		);
	                    	?>
	                    </div>
	                  
	                <div class="form-group">
	                    <label>Subject<span class="require">*</span></label>
		                    <?php
	                    		echo $this->Form->input('subject', array(
	                    			'div' => false,
	                    			'label' => false,
	                    			'class' => 'form-control',
	                    			'default' => $template['EmailTemplate']['subject']
	                    			)
	                    		);
	                    	?>
	                    </div>
	                  <div class="form-group">
	                  <label>Template Body<span class="require">*</span></label>
		            	<?php
				        	echo $this->Form->input('mail_body', 
				        		array(
				        			'type' => 'textarea',
				        			'rows' => 10,
				        			'cols' => 50,
				        			'label' => false,
				        			'div' => false,
				        			'class' => 'textarea form-control',
				        			'id' => 'MailBody',
				        			'default' => $template['EmailTemplate']['mail_body']
				        			)
				        		);
				        	echo $this->Fck->load('MailBody');
				       	?>
		            </div>
		            <div class="form-group">
		            	<input onclick="window.history.back();" type="button" value="Cancel" class="btn btn-default" />
		                <input type="submit" value="Submit" class="btn btn-success" />
		            </div>
		        </div>
		    <?php echo $this->Form->end(); ?>
	    </div>
	</div>
</div>
</section>
</div>
<style type="text/css">
	.box{
		padding: 15px;
	}
</style>
<?php echo $this->Html->script('users/email',array('block' => 'scriptBottom'));?>