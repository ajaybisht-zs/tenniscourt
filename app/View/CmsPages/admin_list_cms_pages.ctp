﻿<div class="content-wrapper">
<?= $this->Flash->render('positive') ?>
    <section class="content-header clearfix">
        <div class="col-lg-6">
            <h1 class="heading-text-color">CMS Listing</h1>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="header-section ">

                    <div id="deleteRecords" class="btn-group delete-staff" style="display:none;">
                        <button class="btn btn-default btn-sm"><i class="ti-trash"></i></button>
                    </div>
                </div>
             <table id="table" class="table table-hover table-striped">
            <thead>
                <tr>
                    <th><?php echo __('Meta Title'); ?></th>
                    <th><?php echo __('Meta Keywords'); ?></th>
                    <th><?php echo __('Page Name'); ?></th>
                    <th><?php echo __('Status'); ?></th>
                    <th><?php echo __('Actions'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(!empty($getAllCmsPages)) { 
                        foreach ($getAllCmsPages as $showAllCmsPages) {
                ?>
                            <tr>
                                <td>
                                    <?php echo $showAllCmsPages['CmsPage']['meta_title']; ?>
                                </td>
                                <td>
                                    <?php echo $showAllCmsPages['CmsPage']['meta_keyword']; ?>
                                </td>
                                <td>
                                    <?php echo $showAllCmsPages['CmsPage']['page_name']; ?>
                                </td>
                                <td>
                <?=($showAllCmsPages['CmsPage']['is_activated'] == 0) ? 'No' : 'Yes'?>

                                </td>
                                <td id="inline-popups">
                                    <?php
                                        echo $this->Html->link(
                                                '<i class="glyphicon glyphicon-edit"></i>',
                                                array(
                                                            'controller' => 'CmsPages',
                                                            'action' => 'editCms',
                                                            base64_encode($showAllCmsPages['CmsPage']['id']),
                                                            'admin' => true
                                                    ),
                                                array(
                                                         
                                                        'escape' => false,
                                                        'title' => 'edit'
                                                    )
                                            );
                                    ?>
                                    <?php
                                        echo $this->Html->link(
                                                ($showAllCmsPages['CmsPage']['is_activated'] ==1)? '  <i class=" glyphicon glyphicon-ok-circle"></i>  ':'  <i class=" glyphicon glyphicon-remove-circle"></i>  ',
                                                '/CmsPages/deactivate/'.base64_encode($showAllCmsPages['CmsPage']['id']),
                                                array(
                                                         
                                                        'escape' => false,
                                                        'confirm'=>($showAllCmsPages['CmsPage']['is_activated'] ==1)? "Are You sure you want to deactivate the page?" : "Are You sure you want to activate the page ?",
                                                        'title'=>($showAllCmsPages['CmsPage']['is_activated'] ==1)? 'Activated':'Deactivated',
                                                    )
                                            );
                                    ?>
                                </td>
                            </tr>
                <?php } } else { ?>
                    <tr>
                        <td colspan="5">
                            No Record Found
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php echo $this->element('backend/pagination'); ?>
        </div>
    </section>
    <!-- /.content -->
</div>