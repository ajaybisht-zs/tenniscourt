<?php
	echo $this->Html->script('fckeditor');
	echo $this->Html->script('backend/AdminSettings/CmsPages/edit_cms', array('block' => 'scriptBottom'));
?>
<div class="content-wrapper">
<?= $this->Flash->render('positive') ?>
    <section class="content-header clearfix">
        <div class="col-lg-6">
            <h1 class="heading-text-color">Manage CMS Pages</h1>
            <div class="text-gry">
                <ol class="breadcrumb " style="margin-bottom: 5px;">
                    <li><?php
                    echo $this->Html->link(
                        '</i><span class="breadcrumb">' . __('CmsPages') . '</span>',
                        array('controller' => 'CmsPages', 'action' => 'listCmsPages','admin' =>true),
                        array(
                            'escape' => false
                        )
                    );
                ?> 	
                </li>
                    <li class="text-gry"><?= __('Edit CmsPages'); ?></li>
                </ol>
            </div>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box">
           <?php
	    		echo $this->Form->create(
	    				'CmsPage',
	    				array(
	    					'url' => '/CmsPages/editCms',
	    					'method' => 'post',
	    					'class' => 'form-horizontal',
	    					'id' => 'EditCms',
	    					'novalidate' => true
	    					)
	    			);
	    			echo $this->Form->hidden('id', array('value' => $cms['CmsPage']['id']));
	    	?>
	            <div class="box-body">
                        <div class="form-group">
	                    <label><?php echo __('Page Title'); ?><span class="require">*</span></label>
	                    
	                    	<?php
	                    		echo $this->Form->input('page_title', array(
	                    			'div' => false,
	                    			'label' => false,
	                    			'class' => 'form-control required',
	                    			'default' => $cms['CmsPage']['page_title']
	                    			)
	                    		);
	                    	?>
	                    </div>
                              <div class="form-group">
	                    <label><?php echo __('Page Name'); ?><span class="require">*</span></label>
	                    </div>
                              <div class="form-group">
	                    	<?php
	                    		echo $this->Form->input('page_name', array(
	                    			'div' => false,
	                    			'label' => false,
	                    			'class' => 'form-control required',
	                    			'default' => $cms['CmsPage']['page_name']
	                    			)
	                    		);
	                    	?>
	                    </div>
                              <div class="form-group">
	                    <label><?php echo __('Meta Title'); ?><span class="require">*</span></label>
	                   
	                    	<?php
	                    		echo $this->Form->input('meta_title', array(
	                    			'div' => false,
	                    			'label' => false,
	                    			'class' => 'form-control required',
	                    			'default' => $cms['CmsPage']['meta_title']
	                    			)
	                    		);
	                    	?>
	                    </div>
                              <div class="form-group">
	                    <label><?php echo __('Meta Keywords'); ?><span class="require">*</span></label>
	                    
	                    	<?php
	                    		echo $this->Form->input('meta_keyword', array(
	                    			'div' => false,
	                    			'label' => false,
	                    			'class' => 'form-control required',
	                    			'default' => $cms['CmsPage']['meta_keyword']
	                    			)
	                    		);
	                    	?>
	                    </div>
                              <div class="form-group">
                              <label><?php echo __('Description'); ?><span class="require">*</span></label>
		            	<?php
				        	echo $this->Form->input('meta_description', 
				        		array(
				        			'type' => 'textarea',
				        			'rows' => 10,
				        			'cols' => 50,
				        			'label' => false,
				        			'div' => false,
				        			'class' => '',
				        			'id' => 'Meta_description',
				        			'default' => $cms['CmsPage']['meta_description']
			        			)
			        		);
			        		
							echo $this->Fck->load('meta_description');
				       	?>
		            </div>
		            <div class="form-actions">
		            <button type="reset" class="btn btn-default">Cancel</button>
		                <input type="submit" value="Submit" class="btn btn-success" />
		            </div>
		        </div>
		    <?php echo $this->Form->end(); ?>
        </div>
    </section>
    <!-- /.content -->
</div>
<?php echo $this->Html->script('users/cms',array('block' => 'scriptBottom'));?>