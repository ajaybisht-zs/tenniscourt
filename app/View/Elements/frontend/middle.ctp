<div id="myCarousel" class="carousel carousel-custom slide carousel-fade" data-ride="carousel">
         <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
         </ol>
         <div class="container">
            <div class="text-section">
               <div class="border-thick">
                  <div class="main-heading-banner">
                     <h2>Because it Pays, to Pay Attention</h2>
                     <h3>The Only Real-Time Notes Marketplace, by Students in Your Classes.</h3>
                     	<?php
                    echo $this->Html->link(
                        'Signup Now',
                        array('controller' => 'Homes', 'action' => 'signup'),
                        array(
                        	'class' => 'btn btn-blue',
                            'escape' => false,
                        )
	                    );
	                     ?>
                  </div>
               </div>
            </div>
         </div>
         <div class="carousel-inner" role="listbox">
            <div class="item active">
               <?php echo $this->Html->image('banner.png',array('alt' => 'slider')); ?>
            </div>
         </div>
         <div class="container chat-icon">
            <button class="btn btn-black">Have Any Questions? <i class=" fa fa-wechat"></i></button>
         </div>
      </div>
 <div class="live-notes clearfix">
         <div class="container text-center">
            <h4 class="text-center">The First Live Notes Marketplace</h4>
            <div class="col-sm-12 col-lg-4">
               <div class="box-border">
                  <?php echo $this->Html->image('live-note.png'); ?>
                  <h5 class="text-purple">The First Live Notes Marketplace</h5>
                  <p>
                     Post your class notes, set your own prices, and make money just for being a great student. Sign up now to begin selling your class notes today.
                  </p>
               </div>
            </div>
            <div class="col-sm-12 col-lg-4">
               <div class="box-border">
                  <?php echo $this->Html->image('only-live-note.png'); ?>
                  <h5 class="text-purple">Only Live Notes</h5>
                  <p>
                     Why pay for stale information? Notes University only lists Live notes from note takers who are 
                     currently enrolled in your classes. 
                  </p>
               </div>
            </div>
            <div class="col-sm-12 col-lg-4">
               <div class="box-border">
                  <?php echo $this->Html->image('quality-controls.png'); ?>
                  <h5 class="text-purple">Strict Quality Controls</h5>
                  <p>
                     Notes University has strict quality standards to ensure our class notes marketplace only offers the best. We actively monitor all notes, and we only list original content from other students like you. 
                  </p>
               </div>
            </div>
         </div>
      </div>


      <div class="container attention">
         <div class="col-sm-7">
            <h3>It Pays to Pay Attention. </h3>
            <ul class="list-inline">
               <li>
                  <i class="fa fa-thumbs-up"></i>There is no limit to what you can earn as a Notes University note taker. You
                  create the notes, you set the prices, and you keep the revenue.
               </li>
               <li>
                  <i class="fa fa-thumbs-up"></i>Notes University charges a seller fee  - that’s all! Everything else is yours to keep.  
               </li>
               <li>
                  <i class="fa fa-thumbs-up"></i>Using our marketplace you can earn 
                  up to $5,000 in a semester!
               </li>
            </ul>
            <button class="btn btn-blue">Read More</button>
         </div>
         <div class="col-sm-5">
            <?php echo $this->Html->image('attention-img.png'); ?>
         </div>
      </div>

      <div class="listen-class">
         <div class="container attention">
            <div class="col-sm-5">
               <?php echo $this->Html->image('Listin-class.png',array('class' => 'img-responsive')); ?>
            </div>
            <div class="col-sm-7">
               <h3>Listen in Class, Don’t Scribble. </h3>
               <ul class="list-inline">
                  <li>
                     <i class="fa fa-thumbs-up"></i>Multitasking doesn’t work. You become less effective when you try to take notes and list to your professor at the same time. 
                  </li>
                  <li>
                     <i class="fa fa-thumbs-up"></i>Our note takers are better than you. Taking great notes takes practice, and our 
                     note takers have the whole class relying upon them to take great notes. Check 
                     out an example of our notes here. 
                  </li>
                  <li>
                     <i class="fa fa-thumbs-up"></i>Accountability. Want to make sure a note taker is paying attention? Just go sit by them in class. 
                  </li>
               </ul>
               	<?php
                    echo $this->Html->link(
                        'Signup Now',
                        array('controller' => 'Homes', 'action' => 'signup'),
                        array(
                        	'class' => 'btn btn-blue',
                            'escape' => false,
                        )
	                    );
	             ?>
              
            </div>
         </div>
      </div>

      <div class="promote">
         <div class="container attention">
            <div class="col-sm-7">
               <h3>Promote and Get Paid.  </h3>
               <ul class="list-inline">
                  <li>
                     <i class="fa fa-thumbs-up"></i>We reward people who help us build our brand and find great people.
                  </li>
                  <li>
                     <i class="fa fa-thumbs-up"></i>By referring a note taker to us, you earn a percentage of what they make for a whole semester! 
                  </li>
                  <li>
                     <i class="fa fa-thumbs-up"></i>There is no limit to what you can make, the more your note takers make, the more you make! 
                  </li>
               </ul>
               <?php
                    echo $this->Html->link(
                        'Signup Now',
                        array('controller' => 'Homes', 'action' => 'signup'),
                        array(
                        	'class' => 'btn btn-blue',
                            'escape' => false,
                        )
	                    );
	             ?>
            </div>
            <div class="col-sm-5">
               <?php echo $this->Html->image('prmote.png'); ?>
            </div>
         </div>
      </div>

      <div class="listen-class signup-bottom">
         <div class="container text-center">
            <h3>Sign up for Notes University today</h3>
            <p>With Note University's live notes marketplace, there is no limit to what you can achieve. </p>
            <div class="button-section">
               <div class="circle-or">or</div>
             
              <?php
                    echo $this->Html->link(
                        'Sign Up Its free',
                        array('controller' => 'Homes', 'action' => 'signup'),
                        array(
                        	'class' => 'btn btn-blue',
                            'escape' => false,
                        )
	                    );
	             ?>
               <?php
                    echo $this->Html->link(
                        'Sign In',
                        array('controller' => 'Homes', 'action' => 'login'),
                        array(
                        	'class' => 'btn btn-purple',
                            'escape' => false,
                        )
                    );
                     ?>
            </div>
         </div>
      </div>
