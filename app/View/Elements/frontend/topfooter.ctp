<div class="footer-bottom">
     <div class="container">
        <div class="col-sm-6 footer-linkss">
           <div class="input-group">
              <input type="text" class="form-control" placeholder="Type Your Email Address">
              <span class="input-group-btn">
              <button class="btn btn-default" type="button"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
              </span>
           </div>
        </div>
        <div class="col-sm-6 footer-linkss">
           <div class="footer-heading">Legal</div>
           <ul class="list-unstyled">
              <li><a href="#">PRIVACY & COOKIES</a></li>
              <li><a href="#">TERMS OF USE</a></li>
              <li><a href="#">TRADEMARKS</a></li>
           </ul>
        </div>
     </div>
  </div>