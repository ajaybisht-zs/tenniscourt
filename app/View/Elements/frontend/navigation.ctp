<nav class="navbar navbar-default nav-top  ">
    <div class="container  ">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
          
            <?php
            echo $this->Html->link(
                '<span class="logo-lg">' . $this->Html->image('logo.png',array('class' =>'img-responsive','alt' => 'logo')) . '</span>',
                array('controller' => 'Homes', 'action' => 'index'),
                [
                    'escape' => false,
                    'class' => 'navbar-brand'
                ]
            );
          ?>
        </div>
        <div id="navbar" class="collapse navbar-collapse ">
            <ul class="nav navbar-nav  navbar-right">
                <li>
                    <?php
                        echo $this->Html->link(
                            'Home',
                            array('controller' => 'Homes', 'action' => 'index'),
                            array(
                                'escape' => false
                            )
                        );
                    ?>
                </li>
                <li>
                    <a href="#about">About Us</a>
                </li>
                <li>
                    <a href="#contact">List Your Venue</a>
                </li>
                <li class="right-nav-log">
                    <?php
                        echo $this->Html->link(
                            'Sign In',
                            array('controller' => 'Homes', 'action' => 'login'),
                            array(
                                'escape' => false
                            )
                        );
                    ?>
                </li>
                <li class="right-nav-log">
                    <?php
                        echo $this->Html->link(
                            'Join Us',
                            array('controller' => 'Homes', 'action' => 'signup'),
                            array(
                                'escape' => false
                            )
                        );
                    ?>
                </li>
            </ul>
        </div>
    </div>
</nav>