<div class="modal Educational-info fade" id="addCourt" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times-circle" aria-hidden="true"></i></span></button>
                <h4 class="modal-title text-center">Add New Court</h4>
            </div>
            <div class="modal-body clearfix">
                <?php echo $this->Form->create('Court',array('url' => array('controller' => 'Homes','action' => 'addCourt'),'type' =>'file')); ?>
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?php
                                    echo $this->Form->input('court_name', array('label' => false, 'div' => false, 'placeholder' => 'Court Name', 'class' => 'form-control'));
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?php
                                    echo $this->Form->input('court_number', array('label' => false, 'div' => false, 'placeholder' => 'Court Number', 'class' => 'form-control'));
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <?php
                                    echo $this->Form->input('contact_person', array('label' => false, 'div' => false, 'placeholder' => 'Contact Person', 'class' => 'form-control'));
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <?php
                                    echo $this->Form->input('contact_phone', array('label' => false, 'div' => false, 'placeholder' => 'Contact Person Phone Number', 'class' => 'form-control'));
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <?php echo $this->Form->input('country_id', array(
                                    'options' => $country,
                                    'class' => 'form-control',
                                    'empty' => 'Select Country',
                                    'label' => false, 
                                    'div' => false
                                ));?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?php
                                    echo $this->Form->input('state', array('label' => false, 'div' => false, 'placeholder' => 'State', 'class' => 'form-control'));
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?php
                                    echo $this->Form->input('zipcode', array('label' => false, 'div' => false, 'placeholder' => 'Zipcode', 'class' => 'form-control'));
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?php echo $this->Form->input('surface_id', array(
                                    'options' => $surface,
                                    'empty' => 'Select Court Surface',
                                    'class' => 'form-control',
                                    'label' => false, 
                                    'div' => false
                                ));?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?php echo $this->Form->input('court_type', array(
                                    'options' => array('Indoor','Outdoor'),
                                    'empty' => 'Court Type',
                                    'class' => 'form-control',
                                    'label' => false, 
                                    'div' => false
                                ));?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <div class="form-group">
                                 <?php
                                    echo $this->Form->input('hourly_rate', array('label' => false, 'div' => false, 'placeholder' => 'Hourly Rate', 'class' => 'form-control'));
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group clearfix">
                            <label class="col-sm-12 Availability">Availability</label>
                            <div class="col-sm-4">
                                <div class="checkbox">
                                    <label><input type="checkbox" name = "data[CourtDay][][day]" value="Monday" checked>Monday</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name = "data[CourtDay][][day]" value="Tuesday">Tuesday</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name = "data[CourtDay][][day]" value="Wednesday">Wednesday</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox">
                                    <label><input type="checkbox" name = "data[CourtDay][][day]" value="Thursday">Thursday</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name = "data[CourtDay][][day]" value="Friday">Friday</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name = "data[CourtDay][][day]" value="Saturday">Saturday</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox">
                                    <label><input type="checkbox" name = "data[CourtDay][][day]" value="Sunday">Sunday</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 form-group">
                        <div class="col-sm-6">
                            <div id="filediv" class="form-group">
                                <input type="file" id="file" name = "data[CourtImage][][image_path]"  class="form-control" placeholder="Add Court Picture">
                                
                            </div>
                            <input type="button" id="add_more" class="upload" value="Add More Files"/>
                        </div>
                       
                    </div>
                    <div class="col-xs-12 form-group">
                        <div class="text-center">
                            <button type="submit" class="btn submit-info btn-green">Submit</button>
                            <button data-dismiss="modal" class="btn submit-info btn-gray">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Html->script('courts/add',array('block' => 'scriptBottom'));?>