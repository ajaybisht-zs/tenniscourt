
<?php echo $this->Form->create('User', array(
                                        'id'    =>  'admin-add-user',
                                        'class' =>  'form-horizontal',
                                        'enctype' => 'multipart/form-data' ));?>
    <div class="form-group">
        <label  class="col-sm-4 control-label">Full Name</label>
        <div class="col-sm-4">
            <?php echo $this->Form->hidden('id',array());?>
            <?php
            echo $this->Form->input('first_name', array('label' => false, 'div' => false, 'placeholder' => 'First Name', 'class' => 'form-control'));
            ?>
        </div>
        <div class="col-sm-4">
            <?php
            echo $this->Form->input('last_name', array('label' => false, 'div' => false, 'placeholder' => 'Last Name', 'class' => 'form-control'));
            ?>
        </div>
    </div>
    <div class="form-group">
        <label  class="col-sm-4 control-label">Email</label>
            <div class="col-sm-8">
                <?php
                echo $this->Form->input('email', array('label' => false, 'div' => false, 'placeholder' => 'email', 'class' => 'form-control'));
                ?>

            </div>
    </div>
    <div class="form-group">
        <label  class="col-sm-4 control-label">Phone Number</label>
            <div class="col-sm-8">
                <?php
                echo $this->Form->input('phone', array('label' => false, 'div' => false, 'data-mask' => '', 'data-inputmask' => '&quot;mask&quot;: &quot;(999) 999-9999&quot;', 'class' => 'form-control','id' => 'edit-phone', 'placeholder' => 'Phone Number'));
                ?>
            </div>
    </div>
    <div class="form-group">
        <label  class="col-sm-4 control-label">Profile Pic</label>
        <div class="col-sm-8">
            <input type="file" name="data[User][profile_pic]" id="file" />
        </div>
    </div>
    <div class="form-group bordered_group"> <label  class="col-sm-4 control-label"></label>
        <div class="col-lg-8">
            <button type="submit" class="btn btn-green">Update</button>
            <button type="button" data-dismiss="modal" class="btn btn-green">Cancel</button>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
            

