<div class="modal fade" id="changePasswordModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

               
                <h4 class="modal-title" id="myModalLabel"><?php echo __('Change Password'); ?></h4>

            </div>

            <div class="modal-body">

                <?php echo $this->Form->create('User', array(

                                'url' => '/Users/changePassword',

                                'type' => 'post',

                                'id' => 'change-password'

                                ));

                    echo $this->Form->hidden('id', array('value' => $this->Session->read('Auth.User.id')));

                ?>   

                    <div class="form-group">

                        <label class="control-label"><?php echo __('Old Password'); ?><span class="require">*</span></label>

                        <?php

                          echo $this->Form->input('old_password', array(

                                        'class' => 'form-control',

                                        'type' => 'password',

                                        'label' => false,

                                        'div' => false,

                                        'placeholder' => 'Old Password'

                                        ));

                        ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo __('New Password'); ?><span class="require">*</span></label>
                        <?php

                            echo $this->Form->input('password', array(

                                        'class' => 'form-control',

                                        'type' => 'password',

                                        'label' => false,

                                        'div' => false,

                                        'placeholder' => 'New Password',

                                        'id' => 'new-password',

                                        'data-display' => 'mainPassword'

                                        ));

                        ?>

                        <div class="left passwordStrength" id="mainPassword"></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><?php echo __('Confirm Password'); ?><span class="require">*</span></label>
                        <?php
                            echo $this->Form->input('confirm_password', array(

                                        'class' => 'form-control',

                                        'type' => 'password',

                                        'label' => false,

                                        'div' => false,

                                        'placeholder' => 'Confirm Password'

                                        ));

                        ?>

                    </div>
                    <div class="form-group">
                        <div class="clearfix">                               
                           <div class="pull-right">
                                <?php

                                    echo $this->Form->button('Close', array(

                                                'class' => 'btn btn-default',

                                                'data-dismiss' => 'modal'

                                                ));
                                    ?>
                                    <?php
                                    echo $this->Form->button('Save', array(

                                                'class' => 'btn btn-primary btn-grad btn-left',

                                                'type' => 'submit'

                                                ));

                                ?>
                            </div>
                        </div> 
                    </div>     
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
<?php
echo $this->Html->script('users/change-password',array('block' => 'scriptBottom'));
?>
