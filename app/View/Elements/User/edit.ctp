<?php echo $this->Form->create('User', array('id' => 'admin-add-user')); ?>
<?php echo $this->Form->hidden('id',array('value' => $user['User']['id']));?>
    <div class="form-group right-inner-addon">
        <?php
        echo $this->Form->input('first_name', array('label' => false, 'div' => false, 'placeholder' => 'First Name', 'class' => 'form-control'));
        ?>
    </div>
    <div class="form-group right-inner-addon">
        <?php
        echo $this->Form->input('last_name', array('label' => false, 'div' => false, 'placeholder' => 'Last Name', 'class' => 'form-control'));
        ?>
    </div>
    <div class="form-group right-inner-addon">
        <?php
        echo $this->Form->input('email', array('label' => false, 'div' => false, 'placeholder' => 'email', 'class' => 'form-control'));
        ?>

    </div>
    <div class="form-group btn-margin-bottom right-inner-addon">
        <button class="btn btn-black btn-block">Update Client</button>
    </div>
    <?php echo $this->Form->end(); ?>