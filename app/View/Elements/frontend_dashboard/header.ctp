      <header class="main-header">
         <!-- Logo -->
         <a href="index.html" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">
            <?php echo $this->Html->image('logo.png');?>
            </span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><?php echo $this->Html->image('logo.png');?></span>
         </a>
         <!-- Header Navbar: style can be found in header.less -->
         <nav class="navbar navbar-static-top nav-custom" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="glyphicon glyphicon-menu-hamburger"></span>
            </a>
            <div class="col-lg-3 col-xs-9">
               <h2 class="top-heading"><i class="fa fa-user" aria-hidden="true"></i> Club Owner</h2>
            </div>
            <div class="col-sm-6  col-md-5 col-lg-3 col-xs-12  search-top">
               <div class="right-inner-addon">
                  <i class="fa fa-search" aria-hidden="true"></i>
                  <input type="search" placeholder="Search here" class="form-control">
               </div>
            </div>
            <div class="navbar-custom-menu">
            <ul class="nav navbar-nav navbar-right top-right-menu">
               <li><a href=""> 
               <i class="fa fa-refresh" aria-hidden="true"></i></a>
               </li>
               <li class="dropdown notifications-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                  <i class="glyphicon glyphicon-bell"></i>
                  <span class="label label-warning">5</span>
                  </a>
                  <ul class="dropdown-menu">
                     <li>
                        <!-- inner menu: contains the actual data -->
                        <div class="slimScrollDiv">
                           <ul class="menu">
                              <li>
                                 <a href="#">
                                 You have 12 new buyer registrations
                                 </a>
                              </li>
                              <li>
                                 <a href="#">
                                 You have 23 new screening requests
                                 </a>
                              </li>
                           </ul>
                     </li>
                  </ul>
               </li>
               <li>
                    <?php
                        echo $this->Html->link(
                            '<i class="fa fa-power-off" aria-hidden="true"></i>',
                            array('controller' => 'Homes', 'action' => 'logout'),
                            array(
                                'escape' => false
                            )
                        );
                    ?>
               </li>
            </ul>
            </div>
         </nav>
      </header>