<script src="http://connect.facebook.net/en_US/all.js"></script>
<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: '1140076159392507', // Set YOUR APP ID
            channelUrl: 'http://localhost/digitalgoods/', // Channel File
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true  // parse XFBML
        });
    }

    (function (d) {
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
    }(document));

    function fbLogin()
    {

        FB.login(function (response) {
            if (response.authResponse)
            {
                document.cookie = "accesstoken=" + response.authResponse['accessToken'];
                saveuserdetail(); // Get User Information.

            } else
            {
                console.log('Authorization failed.');
            }
        }, {scope: 'email,user_photos,user_friends,public_profile'});
    }


    function getUserInfo() {
        FB.api('/me', function (response) {
            console.log(response);
        });
    }

    function saveuserdetail() {
        FB.api('/me', function (response) {
            jQuery.ajax({
                type: "POST",
                url: '<?php echo $this->Html->url(array('controller' => 'homes', 'action' => 'fblogin')) ?>',
                data: response,
                success: function (msg) {
                    if (msg == 1) {
                        window.location.replace('<?php echo $this->Html->url(array('controller' => 'Homes', 'action' => 'index')) ?>');
                    } else if (msg == 0) {
                        alert('Successfully registered!');
                        window.location.replace('<?php echo $this->Html->url(array('controller' => 'homes', 'action' => 'index')) ?>');
                    } else {
                        alert('Something went wrong, Please try again!');
                    }
                },
            });
        });
    }
</script>
