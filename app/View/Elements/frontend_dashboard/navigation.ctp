<aside class="main-sidebar">
         <div class="sidebar">
            <li class="dropdown user user-menu drop-custom">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown">
               <?php echo $this->Html->image('profile-logo.png',array('alt' => 'User Image','class' => 'user-image'));?>
               <span class="hidden-xs hidden-sm">
               Club Admin
               </span>
               <i class="fa fa-angle-down" aria-hidden="true"></i>
               </a>
               <ul aria-labelledby="drop3" class="dropdown-menu left-top-drop">
                  <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i> Profile</a></li>
                  <li><a href="#"><i class="fa fa-cog" aria-hidden="true"></i> Account Settings</a></li>
                  <li>
                    <?php
                        echo $this->Html->link(
                            '<i class="fa fa-sign-out" aria-hidden="true"></i> Logout',
                            array('controller' => 'Homes', 'action' => 'logout'),
                            array(
                                'escape' => false
                            )
                        );
                    ?>
                  </li>
               </ul>
            </li>
            <ul class="sidebar-menu">
               <li class="treeview active">
                  <a href="dashboard.html">
                  <i class="fa fa-tachometer" aria-hidden="true"></i>
                  <span>Dashboard</span>
                  </a>
               </li>
               <li class="treeview">
                  <a href="dashboard.html">
                  <i class="fa fa-book" aria-hidden="true"></i>
                  <span>Booking List</span>
                  </a>
               </li>
               <li class="treeview">
                  <a href="dashboard.html">
                  <i class="fa fa-clone" aria-hidden="true"></i>
                  <span>My Courts</span>
                  </a>
               </li>
               <li class="treeview">
                  <a href="dashboard.html">
                  <i class="fa fa-times-circle" aria-hidden="true"></i>
                  <span>Cancellation Request</span>
                  </a>
               </li>
               <li class="treeview">
                  <a href="dashboard.html">
                  <i class="fa fa-money" aria-hidden="true"></i>
                  <span>My Payments</span>
                  </a>
               </li>
            </ul>
         </div>
      </aside>