<header class="main-header">
         <!-- Logo -->
         <?php
            echo $this->Html->link(
                '<span class="logo-mini">' . $this->Html->image('logo.png') . '</span><span class="logo-lg">' . $this->Html->image('logo.png') . '</span>',
                array('controller' => 'Users', 'action' => 'index'),
                [
                    'escape' => false,
                    'class' => 'logo'
                ]
            );
          ?>
         <!-- Header Navbar: style can be found in header.less -->
         <nav class="navbar navbar-static-top nav-custom" role="navigation">
       <!-- Sidebar toggle button-->
              <div class="navbar-custom-menu">
               <ul class="nav navbar-nav">
                  <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <span class="hidden-xs hidden-sm">
                
                <?php echo "Welcome ".$this->Session->read('Auth.User.first_name')?>
               
                </span>
                <?php if($this->Session->read('Auth.User.profile_pic')){
                    echo $this->Html->image('profile-logo.png',array('class' => "user-image" , 'alt' => "User Image"));
                }else{
                    echo $this->Html->image('profile/'.$this->Session->read('Auth.User.profile_pic'),array('class' => "user-image" , 'alt' => "User Image"));
                }
                ?>
                <i class="fa fa-angle-down" aria-hidden="true"></i>
                </a>
                    <ul aria-labelledby="drop3" class="dropdown-menu">
                       <li>
                        <?php
                            echo $this->Html->link(
                                '<span>' . __('Change password') . '</span>',
                                'javascriptVoid(0)',
                                array(
                                            'data-toggle' => 'modal',
                                            'data-target' => '#changePasswordModal',
                                            'escape' => false
                                    )
                            );
                        ?>
                       </li>
                        <?php if($this->params['prefix'] != 'clubmember'){ ?>
                       <li>
                        <?php echo $this->Html->link(__('Profile'),
                                'javascript:void(0)',
                                array(                                           
                                        'class' => 'editprofile',
                                        'data-url' => $this->Html->url(
                                         [      
                                             'controller' => 'Users',                                                
                                             "action" => "profile"
                                           
                                         ],true
                                     ),
                                        'escape' => false,
                                    )
                        );?>
                       </li>
                       <?php } ?>
                       <li>
                        <?php
                            echo $this->Html->link(
                                '<span>' . __('Logout') . '</span>',
                                array('controller' => 'Users', 'action' => 'logout','clubmember'=> false),
                                array(
                                    'escape' => false
                                )
                            );
                        ?>
                       </li>
                    </ul>
               </ul>
            </div>
         </nav>
      </header>
<?php
echo $this->Html->script('users/profile',array('block' => 'scriptBottom'));
?>