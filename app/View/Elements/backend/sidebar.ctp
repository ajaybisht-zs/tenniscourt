<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            
            <li class="treeview active">
                <?php
                    echo $this->Html->link(
                        '<i class="glyphicon glyphicon-file"></i><span>' . __('Xls Files') . '</span>',
                        array('controller' => 'Users', 'action' => 'xls_index'),
                        array(
                            'escape' => false
                        )
                    );
                ?>
            </li>
           
         
           
        </ul>
    </section>
 <!-- /.sidebar -->
</aside>


