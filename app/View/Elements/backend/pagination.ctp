<?php 
	$model = !empty($paginationOptions) ? $paginationOptions['model'] : null;
	if($this->Paginator->counter('{:pages}') > 1){ ?>
    <div class="col-sm-12">
    <nav class="clearfix">
        <ul class="pagination pull-right">
            <?php
            echo $this->Paginator->prev(__('Previous'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a', 'model' => $model));
            echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1, 'model' => $model));
            echo $this->Paginator->next(__('Next'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a', 'model' => $model));
            ?>
        </ul>
    </nav>
    </div>
<?php } ?>