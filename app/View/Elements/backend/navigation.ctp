<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            
            <li class="treeview active">
                <?php
                    echo $this->Html->link(
                        '<i class="glyphicon glyphicon-group"></i><span>' . __('Manage Club Admin') . '</span>',
                        array('controller' => 'Users', 'action' => 'admin_index','admin' => true),
                        array(
                            'escape' => false
                        )
                    );
                ?>
            </li>
            <li class="treeview active">
                <?php
                    echo $this->Html->link(
                        '<i class="glyphicon glyphicon-book"></i><span>' . __('Manage CMS pages') . '</span>',
                        array('controller' => 'CmsPages', 'action' => 'admin_listCmsPages','admin' => true),
                        array(
                            'escape' => false
                        )
                    );
                ?>
            </li>
            <li class="treeview active">
                <?php
                    echo $this->Html->link(
                        '<i class="glyphicon glyphicon-envelope"></i><span>' . __('Manage Email Templates') . '</span>',
                        array('controller' => 'EmailTemplates', 'action' => 'admin_index','admin' => true),
                        array(
                            'escape' => false
                        )
                    );
                ?>
            </li>
         
           
        </ul>
    </section>
 <!-- /.sidebar -->
</aside>