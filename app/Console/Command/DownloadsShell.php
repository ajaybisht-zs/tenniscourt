<?php 
App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('Controller', 'Controller');
App::uses('AppController', 'Controller');
App::uses('ComponentCollection', 'Controller');
App::uses('Ftp', 'Ftp.Model');
class DownloadsShell extends AppShell {
    public $uses = array('User','File');
	public function main() {
		ini_set('memory_limit', '1024M');
		set_time_limit(0);
		$Ftp = new Ftp();
        $users = $this->User->find('all',array( 
            'conditions' => array(
                'User.user_role_id' => 2
                )
            )
        );

        foreach ($users as $key => $user) {
            $connected = $Ftp->connect(array(
                'host' => $user['User']['ftp_host'],
                'username' => $user['User']['ftp_user'],
                'password' => $user['User']['ftp_pass']
            ));
        try {
            $files = $Ftp->find('all');
                if(!empty($files)){
                   foreach ($files as $key => $file) {
                    if($file['Ftp']['is_dir'] == 0) { 
                        $file_parts = pathinfo($file['Ftp']['filename']);
                        if($file_parts['extension'] == 'xls') {
                            $xlsfile = $file['Ftp']['filename'];
                            $fileDetails =  array(
                                'File' => array(
                                    'filename' => $xlsfile,
                                    'user_id' => $user['User']['id'],
                                    'download_date' => date('Y-m-d')
                                    )
                                );
                            $file = $this->loadModel('File');
                            $this->File->create();
                            $this->File->save($fileDetails);
                            $Ftp->save(array(
                            'local' => '/home/damanjit/Downloads/ftp/'.$xlsfile,
                            'remote' => '/'.$xlsfile,
                            'direction' => 'down',
                            ));
                        }
                    }

                } 
            }
        } catch (Exception $e) {
            debug($e);
        }
    }
        
	}
}