$(document).ready(function() {
  
  // page is now ready, initialize the calendar...
    facUrl = $('#admin-calendar').attr("data-href");
     $('#admin-calendar').fullCalendar({
      theme:false,
      header: {
       left: 'prev,next today',
       center: 'title',
       right: 'month,agendaWeek,agendaDay',

      },
      buttonText: {
       today: 'today',
       month: 'month',
       week: 'week',
       day: 'day'
      },
      
      events: facUrl,
      eventClick: function(event, element) {
      getStudent(event) ;
      },
      eventRender: function(event, element){
       var content = "<p class='p-font'>"+event.label+": "+event.title+"<br>"+event.subject+"<br>Time: "+ event.time+"</p>";
       //alert(content);
       element.html(content);
       
      },
      editable: false,
      droppable: true, 
     });

  });