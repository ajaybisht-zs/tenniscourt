(function(){
     var addEmployeeValidate = function(ele){
        console.log($(ele));
        $(ele).validate({
            rules: {
                "data[User][first_name]": {
                  required: true,
                        },
                "data[User][last_name]" : {
                  required: true
                },
                "data[User][email]" : {
                  required: true,
                  email : true
                },
                
                "data[User][password]" : {
                  required: true,
                },
                "data[User][password_confirm]" :{
                    required : true,
                    equalTo : "#UserPassword"
                },
                "data[User][ftp_host]" :{
                    required : true
                },
                "data[User][ftp_user]" :{
                    required : true
                },
                "data[User][ftp_pass]" :{
                    required : true
                },
                "data[User][dropbox_access_token]" :{
                    required : true
                },
                "data[User][quick_access_token]" :{
                    required : true
                },
                "data[User][quick_access_secret]" :{
                    required : true
                },
                "data[User][quick_consumer]" :{
                    required : true
                },
                "data[User][quick_consumer_secret]" :{
                    required : true
                }
            },
            messages: { 
                "data[User][first_name]": {
                  required: "Enter the First name",
                        },     
                "data[User][last_name]" : {
                required: "Enter the Last name",
                },
                "data[User][email]" : {
                    required: "Enter the email",
                    email : "Enter valid email address"
                },
               
                "data[User][password]" : {
                    required : "Enter Password"
                },
                "data[User][password_confirm]" : {
                    required : "Enter the confirm password",
                    equalTo : "password and confirm password do no match"
                },
                "data[User][ftp_host]" :{
                    required : "Enter the Ftp host"
                },
                "data[User][ftp_user]" :{
                    required : "Enter the Ftp Username"
                },
                "data[User][ftp_pass]" :{
                    required : "Enter the Ftp password"
                },
                "data[User][dropbox_access_token]" :{
                    required : "Enter the dropbox access token"
                },
                "data[User][quick_access_token]" :{
                    required : "Enter the Quickbook access token"
                },
                "data[User][quick_access_secret]" :{
                    required : "Enter the Quickbook access token secret"
                },
                "data[User][quick_consumer]" :{
                    required : "Enter the Quickbook Consumer key"
                },
                "data[User][quick_consumer_secret]" :{
                    required : "Enter the quickbook consumer secret key"
                }
            }
        })
    };
    addEmployeeValidate('#admin-add-user');
    var viewUserData = function(url) {
       $.ajax({
           url: url,
           success:function( data ) {
                 $('#view-client').find('.modal-body').html(data);
                 $('#view-client').modal('show');
           }
       });
   };
   $('.viewEmployee').on('click',function(){
      var url = $(this).attr('data-url');
      viewUserData(url);
   });
   $('.editEmployee').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
                type:"GET",
                url:url,        
                success: function(data){
                    console.log(data);
                    $('#edit-client').find('.modal-body').html(data);
                    $('#edit-client').modal('show');
                    
                },
               
            });
    });
  

})();