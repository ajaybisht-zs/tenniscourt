(function () {
	var changePasswordValidate = function(ele){
		console.log($(ele));
		$(ele).validate({
			rules: {
				"first_name": {
                    required: true
                },
                "last_name": {
                    required: true
                    
                },
                "email": {
                    required: true
                },
                "phone": {
                    required: true,
                    maxlength: 10,
                    digits:true
                },
                "message": {
                    required: true,
                    
                },
                
			},
		   messages: {		  
		
		   }
		});
	};
	changePasswordValidate('#UserContactForm');
	
})();
