(function () {
	$('#apply-job-form').validate({
		rules: {
			"data[JobApplication][first_name]": {
                required: true
            },
            "data[JobApplication][last_name]": {
                required: true
            },
            "data[JobApplication][email]": {
                required: true
            },
            "data[JobApplication][phone]": {
                required: true,
                maxlength: 10,
                digits:true
            },
            "data[JobApplication][qualification]": {
                required: true
            },
            "data[JobApplication][experience]": {
                required: true
            }
		}
	});
})();
