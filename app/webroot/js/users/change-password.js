(function () {
	var changePasswordValidate = function(ele){
		console.log($(ele));
		$(ele).validate({
			rules: {
				"data[User][old_password]": {
                    required: true
                },
                "data[User][password]": {
                    required: true,
                    minlength: 6
                },
                "data[User][confirm_password]": {
                    equalTo: '#new-password'
                }
			},
		   messages: {		  
		
		   }
		});
	};
	changePasswordValidate('#change-password');
	$('#changePasswordModal').on('shown.bs.modal', function () {
	  	changePasswordValidate('#change-password');

	})
})();
