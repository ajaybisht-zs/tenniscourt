(function () {
	var changePasswordValidate = function(ele){
		$(ele).validate({
			rules: {
				"data[User][first_name]": {
                    required: true
                },
                "data[User][last_name]": {
                    required: true
                    
                },
                "data[User][email]": {
                    required: true
                },
                "data[User][phone]": {
                    required: true,
                    maxlength: 10,
                    digits:true
                },
                "data[User][password]": {
                    required: true,
                    minlength:6
                },
                "data[User][password_confirm]": {
                    required: true,
                     minlength:6,
                     equalTo:"#UserPassword"
                }
			},
		   messages: {		  
		
		   }
		});
	};
    var registerValidate = function(ele){
        console.log($(ele));
        $(ele).validate({
            rules: {
                "data[User][first_name]": {
                    required: true
                },
                "data[User][last_name]": {
                    required: true
                    
                },
                "data[User][email]": {
                    required: true
                },
                "data[User][phone]": {
                    required: true,
                    maxlength: 10,
                    digits:true
                },
                "data[User][password]": {
                    required: true,
                    minlength:6
                },
                "data[User][password_confirm]": {
                    required: true,
                     minlength:6,
                     equalTo:"#UserPassword"
                }
            },
           messages: {        
        
           }
        });
    };
    var forgotPassword = function(ele){
        console.log($(ele));
        $(ele).validate({
            rules: {
                "data[User][email]": {
                    required: true
                },
            },
           messages: {        
        
           }
        });
    };
	changePasswordValidate('#forgotpassword-form');
    changePasswordValidate('#UserAdminSubeditForm');
    changePasswordValidate('#admin-add-user');
	registerValidate('#UserSignupForm');
})();
