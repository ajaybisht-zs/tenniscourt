$(function () {
   var validate_login = function(element) {
      element.validate({
         rules: {
            'data[User][email]': {
               'required': true,
               'email': true
            },
            'data[User][password]': {
               'required': true
            }
         },
         messages: {
            'email': {
               'required': 'Please enter an email address.',
               'email': 'Please enter a valid email address.'
            },
            'password': {
               'required': 'Please enter password.'
            },
            
         },
         errorPlacement: function(error, element) {
            error.insertAfter(element.parents('.input-group'));
         }
      });
   }  

   validate_login($('#login-form'));
   var validate_signup = function(element) {
      element.validate({
         rules: {
            'data[User][email]': {
               'required': true,
               'email': true
            },
            'data[User][first_name]' : {
               required : true
            },
           'data[User][last_name]' : {
               required : true
            },
           'data[UserProfile][phone]' :{
               required : true,
               digit : true,
               maxlength : 10
            },
            'data[UserProfile][country]' : {
                required : true
            },
            'data[UserProfile][state]' : {
                required : true
            },
            'data[UserProfile][zipcode]' :{
                required : true,
                maxlength : 6
            },
            'data[User][password]' : {
                required : true,
                minlength : 6
            },
            'data[User][confirm_password]' : {
                required : true
                equalTo: '#conf-pass'
            }
          
         }, 
         messages: {
            'data[User][email]': {
               'required': "Enter the email",
               'email': "Enter the valid format"
            },
            'data[User][first_name]' : {
               required : "Enter the first name"
            },
           'data[User][last_name]' : {
               required : "Enter the last name"
            },
           'data[UserProfile][phone]' :{
               required : "Enter the phone number",
               digit : "Enter digits only",
               maxlength : "Phone number must not be more than 10 characters"
            },
            'data[UserProfile][country]' : {
                required : "Enter country"
            },
            'data[UserProfile][state]' : {
                required : "Enter state"
            },
            'data[UserProfile][zipcode]' :{
                required : "Enter Zipcode",
                maxlength : "Zipcode must not be greater than 6 digits"
            },
            'data[User][password]' : {
                required : "Enter password",
                minlength : "Password must not be greater than 6 digits"
            },
            'data[User][confirm_password]' : {
                required : "Enter confirm password"
                equalTo: 'Confirm password and password must be equal'
            }
         },
         errorPlacement: function(error, element) {
            error.insertAfter(element.parents('.form-group'));
         }
      });
   }  

   validate_signup($('#signupform'));
   var validate_forgot = function(element) {
      element.validate({
         rules: {
            'data[User][email]': {
               'required': true,
               'email': true
            }
          
         }, 
         messages: {
            'data[User][email]': {
               'required': "Enter the email",
               'email': "Enter the valid format"
            }
          
         },
         errorPlacement: function(error, element) {
            error.insertAfter(element.parents('.form-group'));
         }
      });
   }  

   validate_forgot($('#forgotpassword-form'));
   var validate_recover = function(element) {
      element.validate({
         rules: {
            'data[User][password]' : {
                required : true,
                minlength : 6
            },
            'data[User][new_password]' : {
                required : true
                equalTo: '#recoverconfirm'
            }
          
         }, 
         messages: {
            'data[User][password]' : {
                required : "Enter password",
                minlength : "Password must not be greater than 6 digits"
            },
            'data[User][new_password]' : {
                required : "Enter confirm password"
                equalTo: 'Confirm password and password must be equal'
            }
          
         },
         errorPlacement: function(error, element) {
            error.insertAfter(element.parents('.form-group'));
         }
      });
   }  

   validate_forgot($('#recoverpassword-form'));
});




   