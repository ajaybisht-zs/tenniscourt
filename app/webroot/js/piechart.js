
   $(function () {
    $('#container').highcharts({

        colors: ['#bcbef0'],
        chart: {
            type: 'areaspline',
             backgroundColor: '#fff',
      style: {
         fontFamily: "Raleway', sans-serif"
      }
        },
        title: {
            text: 'Notes Purchase Summary Chart '
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 150,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: ''
        },
        xAxis: {
            categories: [
                'Mar 3',
                'Mar 7',
                'Mar 11',
                'Mar 15',
                'Mar 19',
                'Mar 23',
                'Mar 27',
                
            ],
            plotBands: [{ // visualize the weekend
                from: 4.5,
                to: 6.5,
                color: '#fff'
            }]
        },
        yAxis: {
            title: {
                text: ''
            }
        },

        tooltip: {
            shared: true,
            valueSuffix: ' units'
        },

        credits: {
            enabled: false
        },
        
        plotOptions: {
            areaspline: {
                fillOpacity: 0.5
            }
        },
        series: [{
            name: 'John',
            data: [3, 4, 3, 5, 4, 10, 12]
        }]
    });
});

// Add the background image to the container
Highcharts.wrap(Highcharts.Chart.prototype, 'getContainer', function (proceed) {
   proceed.call(this);
   this.container.style.background = 'url(http://www.highcharts.com/samples/graphics/sand.png)';
});