(function () {
   
  // validate signup form on keyup and submit
  var validateSignUp = function(element) {

      $.validator.addMethod("passwordRegex", function (value, element) {
          return this.optional(element) || /^((?=.*\d)|(?=.*["!#$%&'()*+,\-:;.<=>?@{|}~^_`\[\]\\]))(?=.*[a-zA-Z]).+$/i.test(value);
      }, "Use at least 1 letter & number or symbol.");
    
      $(element).validate({
        rules: {
           
            'employee-name': {
              required: true,
            },

            'last-name': {
              required: true,
            },

              'id': {
              required: true,
            },

            'select-department': {
              required: true,
            },

             'email': {
              required: true,
              email: true
            },

             'phone': {
              required: true,
            },

            'data-space': {
              required: true,
            },

             'password': {
              required: true,
              minlength:6
            },

             'cpassword': {
              required: true,
              minlength:6,
              equalTo:"#password"
            },
     

        },
        messages: {
            'data[User][email]': {
              required: "Please enter a valid email address"
            },

            'data[User][lastname]': {
              required: "Please provide a password",
              minlength: "Your password must be at least 9 characters long"
            },
    
        }
      });
    };

  $(document).ready(function() {
      console.log('sdhas');
        validateSignUp(".manage-employee");
    });

//   $(window).ready(function(){
//    var equalto =$("body").innerHeight();
//    $(".main-sidebar").innerHeight(equalto);

   
 
// });

   

})();


