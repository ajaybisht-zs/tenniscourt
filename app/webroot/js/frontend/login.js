(function () {
   
  // validate signup form on keyup and submit
  var validateSignUp = function(element) {

      $.validator.addMethod("passwordRegex", function (value, element) {
          return this.optional(element) || /^((?=.*\d)|(?=.*["!#$%&'()*+,\-:;.<=>?@{|}~^_`\[\]\\]))(?=.*[a-zA-Z]).+$/i.test(value);
      }, "Use at least 1 letter & number or symbol.");
    
      $(element).validate({
        rules: {         
             'email': {
              required: true,
              email: true
            },

             'password': {
              required: true,
              minlength:6
            },
        },

        messages: {
            'data[User][email]': {
              required: "Please enter a valid email address"
            },

            'data[User][lastname]': {
              required: "Please provide a password",
              minlength: "Your password must be at least 9 characters long"
            },  
        }

      });
    };

  $(document).ready(function() {
      // console.log('sdhas');
    validateSignUp("#signupform");
  });

})();


