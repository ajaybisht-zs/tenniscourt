$(function () {
   var validate_addcourt = function(element) {
      element.validate({
         rules: {
            'data[Court][court_name]': {
               'required': true,
            },
            'data[Court][court_number]': {
               'required': true
            },
            'data[Court][contact_person]' : {
                required : true
            },
            'data[Court][contact_phone]' : {
                required : true
            },
            'data[Court][country_id]' : {
                required : true
            },
            'data[Court][state]' : {
                required : true
            },
            'data[Court][zipcode]' : {
                required : true,
                maxlength : 6
            },
            'data[Court][surface_id]' : {
                required : true
            },
            'data[Court][court_type]' : {
                required : true
            },
            'data[Court][hourly_rate]' : {
                required : true
            }

         },
         messages: {
            'data[Court][court_name]': {
               'required': "Enter the court name",
            },
            'data[Court][court_number]': {
               'required': "Enter the court number"
            },
            'data[Court][contact_person]' : {
                required : "Enter the contact person name"
            },
            'data[Court][contact_phone]' : {
                required : "Enter the contact person number"
            },
            'data[Court][country_id]' : {
                required : "Select country"
            },
            'data[Court][state]' : {
                required : "Enter the state"
            },
            'data[Court][zipcode]' : {
                required : "Enter the zipcode",
                maxlength : "Zipcode must not be greater than 6 digits"
            },
            'data[Court][surface_id]' : {
                required : "Select the court surface"
            },
            'data[Court][court_type]' : {
                required : "Select court type"
            },
            'data[Court][hourly_rate]' : {
                required : "Enter hourly rate"
            }
            
         },
         errorPlacement: function(error, element) {
            error.insertAfter(element.parents('.form-group'));
         }
      });
   }  

   validate_addcourt($('#CourtAddCourtForm'));
  
});




   