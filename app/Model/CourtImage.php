<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 */
class CourtImage extends AppModel {
    public $actsAs = array(
        'Containable',
         'CakeAttachment.Upload' => array(
             'image_path' => array(
                 'dir' => "{IMAGES}Courts",
                 // 'deleteMainFile' => true,
                 //thumbnails declaration
                 // 'thumbsizes' => array(
                 //     // 'main' => array('width' => 800, 'height' => 600, 'name' =>  'main.{$file}.{$ext}'),
                 //     // 'preview' => array('width' => 200, 'height' => 200, 'name' => 'preview.{$file}.{$ext}'),
                 //     // 'thumb' => array('width' => 100, 'height' => 100, 'name' =>  'thumb.{$file}.{$ext}', 'proportional' => false)
                 // )
             )
         )
 );

}