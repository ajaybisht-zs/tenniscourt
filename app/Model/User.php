<?php
App::uses('AppModel', 'Model');
App::uses('Dropbox', 'Dropbox.Model');	
/**
 * User Model
 *
 */
class User extends AppModel {
  public $hasOne = array(
        'UserProfile' => array(
          'className' => 'UserProfile',
          'foreignKey' => 'user_id',
          'conditions' => '',
          'fields' => '',
          'order' => '',
          'dependent' => true
        )
    );
  public $hasMany = array(
        'Court' => array(
            'className' => 'Court',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'dependent' => true
            )

    );
	public function beforeSave($options = array()) {
	   if (isset($this->data[$this->alias]['password'])) {
	       $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
	   }
	   return true;
	}
	
  public $dropbox_token = 'SET THESE TO YOUR TOKENS';
  public $dropbox_token_secret = 'SET THESE TO YOUR TOKENS';

  public function complex() {
    $info = $this->account_info();
    $this->cp(array(
      'from_path' => 'Copy/This/File.zip',
      'to_path' => 'To/This/File2.zip',
    ));
    return $this->ls('To/This/');
  }

}
