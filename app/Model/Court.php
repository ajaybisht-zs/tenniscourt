<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 */
class Court extends AppModel {
  
  public $actsAs = array(
        'Containable'
        
 );
  public $hasMany = array(
        'CourtDay' => array(
            'className' => 'CourtDay',
            'foreignKey' => 'court_id',
            'dependent' => true
        ),
        'CourtImage' => array(
            'className' => 'CourtImage',
            'foreignKey' => 'court_id',
            'dependent' => true
            )
    );
  public $belongsTo = array(
        'Surface' => array(
            'className' => 'Surface',
            'foreignKey' => 'surface_id',
            'dependent' => true
        )
    );
}
