<?php
App::uses('AppModel', 'Model');
/**
 * Invitation Model
 *
 * @property Clubmember $Clubmember
 * @property Tournament $Tournament
 */
class UserProfile extends AppModel {


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	
	public $hasOne = array(
		'UserProfile' => array(
			'className' => 'UserProfile',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'dependent' => true
		)
		);
}
