<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('Ftp', 'Ftp.Model');
App::import('Vendor', 'php-excel-reader/excel_reader2');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class UsersController extends AppController {

	public $components = array('Flash','Paginator','RequestHandler');
  public function beforeFilter() {
      $this->Auth->allow('admin_login');
     }
	public function admin_login() {
		$this->layout = 'login';

			if ($this->request->is('post')) {
	        if ($this->Auth->login()) {
                if($this->Auth->user('user_role_id') == 1) {
                  return $this->redirect(array('controller' => 'Users','action' => 'index','admin' => true));  
                }
	            else {
                    return $this->redirect(array('controller' => 'Homes','action' => 'home'));
                }
	     
	        }
	        $this->Flash->error(
	            __('Username or password is incorrect')
	        );
	   $this->redirect($this->referer());
	    }
	}
	public function admin_logout() {
        $this->Auth->logout();
        if($this->params['prefix'] == 'admin'){

            return $this->redirect(array('controller' => 'Users','action' => 'login','admin' => true));
        }
	}
    public function admin_index(){
        $this->layout = 'backend';
        $this->layout = 'backend';
        $this->loadModel('User');    
        $conditions = array('User.is_deleted'=>0,'User.user_role_id' => 2);    
        if (isset($this->request->query) && !empty($this->request->query)) {
            $searchData = array(
                'OR' => array(
                    'User.first_name LIKE' => '%'. $this->request->query['search'] .'%',
                    'User.last_name LIKE' => '%'. $this->request->query['search'] .'%',
                    'User.email LIKE' => '%'. $this->request->query['search'] .'%'
                    )
                );
            $conditions = array_merge($conditions, $searchData);
        }
        $this->Paginator->settings = array(
                            'conditions' => $conditions,
                            'limit' => 10,
                            'order' => 'User.created desc'
                        );
        $users = $this->Paginator->paginate('User');
        $this->set(compact(['users']));
    }
    public function admin_add() {
        $this->layout = "backend"; 
        $this->loadModel('User');
        if ($this->request->is('post')) {
            $this->request->data['User']['user_role_id'] = 2;
            if ($this->User->save($this->request->data)) {
                $this->Flash->success('Client has been successfully added', array(
                                                            'key' => 'positive'
                                                        )
                 );
            } else {
                $this->Flash->error('The Client could not be created. Please, try again.', array(
                                                            'key' => 'positive'
                                                        )
                                );
            }
            return $this->redirect(array(
                    'controller' => 'Users',
                    'action' => 'admin_index',
                    'admin' => true
                    ));
            }
    }
    public function admin_editEmployee($id) {
        $this->layout = false;
        $this->loadModel('User');
        $user_id = base64_decode($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['User']['id'] = $user_id;
           if ($this->User->save($this->request->data)) {
               $this->Flash->success(__('Client has been Updated.'),array(
                                                                    'key' => 'positive'
                                                                ));
               return $this->redirect($this->referer());
           }
        }
        $user = $this->User->find('first',array('conditions' => array(
                    'User.id' => $user_id,
                )));
        $this->set(compact('user'));
        $this->request->data = $user;
        if($this->request->is('ajax')){
            $this->autoRender = false;
            $this->render('/Elements/User/edit');
        }   

    }
    public function admin_viewEmployee($id) {
        $this->layout = false;
        $this->loadModel('User');
        $user_id = base64_decode($id);
        $user = $this->User->find('first',array('conditions' => array(
                    'User.id' => $user_id,
                )
            )
        );
        $this->set(compact('user'));
        $this->request->data = $user;
        if($this->request->is('ajax')){
            $this->autoRender = false;
            $this->render('/Elements/User/view');
        }  
    }
    
    public function admin_changePassword() {
        $this->layout = "backend";
        $this->loadModel('User');
        if ($this->request->is('post')  && !empty($this->request->data['User']['password'])) {            
            if($this->request->data['User']['password'] !== $this->request->data['User']['confirm_password']) {
               $this->Flash->error('Password and confirm password did not matched');
               return $this->redirect($this->referer());
            }
           $users = $this->User->find('first',array('conditions'=> array('id' => $this->Auth->user('id') )));   
           if ($users['User']['password'] != AuthComponent::password($this->request->data['User']['old_password'])) {                
               $this->Flash->error('Old Password did not matched', array(
                                                            'key' => 'positive'
                                                        )
                 );
               return $this->redirect($this->referer());
            }

           $this->request->data['User']['id'] = $this->Auth->user('id');
           if ($this->User->save($this->request->data)) {
                $this->Flash->success('Password has been updated succesfully.', array(
                                                            'key' => 'positive'
                                                        )
                 );
               return $this->redirect($this->referer());
           } else {
                $this->Flash->error('Something went wrong. Please try again.', array(
                                                            'key' => 'positive'
                                                        )
                 );
               return $this->redirect($this->referer());
           }
       }
    }
    public function admin_profile() {
       $this->loadModel('User');
        if ($this->request->is(['patch', 'post', 'put'])) {
            // pr($this->request->data);die;
          if($this->request->data['User']['profile_pic']['error'] == 0 ) {
          $ext = explode('.', $this->request->data['User']['profile_pic']['name']);
          $fileName = CakeText::uuid(). '.' . end($ext);
          move_uploaded_file($this->request->data['User']['profile_pic']['tmp_name'],WWW_ROOT.'/img/profile/' . $fileName) ;
          $this->request->data['User']['profile_pic'] = $fileName; 
          }else{
            unset($this->request->data['User']['profile_pic']); 
          }
          
           $this->request->data['User']['id'] = $this->Auth->user('id');
           if ($this->User->save($this->request->data)) {
                $authData = $this->Auth->user();
                $authData['User']['first_name'] = $this->request->data['User']['first_name'];
                $authData['User']['last_name'] = $this->request->data['User']['last_name'];
                if(isset($fileName)){
                    $authData['User']['profile_pic'] = $this->request->data['User']['profile_pic'];
                }
                $authData['User']['email'] = $this->request->data['User']['email'];
                $this->Session->write('Auth', $this->User->read(null, $this->Auth->User('id')));
                $this->Flash->success(__('Employee has been Updated.'),array(
                                                                    'key' => 'positive'
                                                                ));
               return $this->redirect($this->referer());
           }
       }
       $this->loadModel('User');
        $user = $this->User->find('first',array('conditions' => array('User.id' => $this->Auth->user('id'))));
        $this->set(compact('user'));
        $this->request->data = $user;
        if($this->request->is('ajax')){
             $this->autoRender = false;
            $this->render('/Elements/User/profile');
        } 

    }
    
}