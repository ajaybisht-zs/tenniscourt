<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('Ftp', 'Ftp.Model');
App::import('Vendor', 'php-excel-reader/excel_reader2');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class HomesController extends AppController {

	public $components = array('Flash','Paginator','RequestHandler');
     public function beforeFilter() {
      $this->Auth->allow('signup','login','index', 'addCourt','logout');
     }
	public function login() {
		$this->layout = 'frontend_layout';
			if ($this->request->is('post')) {
        	        if ($this->Auth->login()) {
                        if($this->Auth->user('is_active') == 1) {
                        
                            return $this->redirect(array('controller' => 'Homes','action' => 'courtListing'));
                    }
                    else {
                        $this->Auth->logout();
                        $this->Flash->success('Please activate your account before login', array(
                                                            'key' => 'positive'
                                                        )
                        );
                        $this->redirect($this->referer()); 
                    }
            }
	        
	        }
	        $this->Flash->error(
	            __('Username or password is incorrect')
	        );
	       
	    }

	public function logout() {
        $this->Auth->logout();
        if($this->params['prefix'] == 'admin'){

            return $this->redirect(array('controller' => 'Users','action' => 'login','admin' => true));
        }
        else{
          return $this->redirect(array('action' => 'login'));
        }
	}
    public function index(){
        $this->layout = 'layout_home';
       
    }
    public function signup() {
        $this->layout = "frontend_layout"; 
        $this->loadModel('User');
        $this->loadModel('UserProfiles');
        if ($this->request->is('post')) { 
            $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
                $string = '';
                 for ($i = 0; $i < 7; $i++) {
                      $string .= $characters[rand(0, strlen($characters) - 1)];
                 }
            $referal= strtoupper($string);
            $this->request->data['User']['token'] = $referal;
            if ($this->User->saveAssociated($this->request->data)) {

                $this->loadModel('EmailTemplate');
                //Fake Template : in future we can replace with our template
                    $temp = $this->EmailTemplate->find('first', array(
                    'conditions'=>array('EmailTemplate.id'=>2)
                    )
                 );
                $link = Router::url(
                        array('controller' => 'Homes', 'action' => 'activate',$this->request->data['User']['token']),
                        true
                    );
                $temp['EmailTemplate']['mail_body'] = str_replace(
                    array('#NAME','#LINK'),
                    array(
                        $this->request->data['User']['first_name']. ' ' .$this->request->data['User']['last_name'],
                        $link
                    ), 
                    $temp['EmailTemplate']['mail_body']
                );
                
                $this->_sendEmailMessage($this->request->data['User']['email'], $temp['EmailTemplate']['mail_body'], $temp['EmailTemplate']['subject']);
                $this->Flash->success('you have been successfully registered.Check you email to activate you account', array(
                                                            'key' => 'positive'
                                                        )
                 );
            } else {
                $this->Flash->error('Registration has been Failed. Please, try again.', array(
                                                            'key' => 'positive'
                                                        )
                                );
            }

            $this->redirect($this->referer());
            }
    }
    public function activate($token) {
        $this->loadModel('User');
        $user = $this->User->find('first',
            array(
                'conditions' => array(
                    'User.token' => $token
                    )
                )
            );
        if(!empty($user)) {
            $this->User->id = $user['User']['id'];
            $this->User->saveField('is_active', 1);
            $this->redirect(array('action' => 'login'));
        }
    }
    public function forgotPassword() {
        $this->layout = "frontend_layout";
        $this->loadModel('User');
        if($this->request->is('post')){
            if (empty($this->request->data)) {
                throw new NotFoundException;
            }
            $data = $this->request->data;
            if(!empty($data['User']['email'])){
                $user = $this->User->find(
                    'first',
                    array(
                        'conditions'=>array(
                            'User.email'=>$data['User']['email']
                            )
                        )
                    );                
                    
                    $key = $user['User']['token'];
                           $link = Router::url(array(
                           'controller' => 'Homes',
                           'action' => 'recoverPassword',
                           $key,
                            ),
                           true
                        );
                        $this->loadModel('EmailTemplate');
                        $temp = $this->EmailTemplate->find('first',array('conditions' => array('EmailTemplate.id' => 3
                                                                                                )
                                                                        )
                                                          );
                        
                          $temp['EmailTemplate']['mail_body'] = str_replace(
                            array('#NAME','#LINK'),
                            array($user['User']['first_name']. ' ' .$user['User']['last_name'],$link), $temp['EmailTemplate']['mail_body']);
                          // pr($temp['EmailTemplate']['mail_body']);die;
                          $this->_sendEmailMessage($data['User']['email'], $temp['EmailTemplate']['mail_body'], $temp['EmailTemplate']['subject']);
                          $this->Flash->success('Forgot password link has been sent to your registered email account.', array(
                                                            'key' => 'positive'
                                                        )
                            );
                        $this->redirect($this->referer());
                                     
                }
                $this->Flash->error('Email not exists Please provide correct email?.', array(
                                                            'key' => 'positive'
                                                        )
                 );
                $this->redirect(['action'=>'login']);
            }            
        }
        public function recoverPassword(){
        $this->layout = "frontend_layout";
        $this->loadModel('User');
        if($this->request->is('post')){
            if(isset($this->request->data['User']['token']) ){
                $user = $this->User->find('first',array('conditions' => array('User.token' => $this->request->data['User']['token']) ));
                if(empty($user)){
                    $this->Flash->error('Link has been expired please Try again.', array(
                                                            'key' => 'positive'
                                                        )
                 );
                    return $this->redirect($this->referer());
                }

                        
            }            
            
            if(!empty($this->request->data['User']['password']) && $this->request->data['User']['password'] == $this->request->data['User']['new_password']){
                //$data['password'] = $this->request->data['password'];
                $this->User->id = $user['User']['id'];
                if( $this->User->saveField('password',  $this->request->data['User']['password']) ) {
                    $this->Flash->success('Password has been changed successfully.Login with your new password', array(
                                                            'key' => 'positive'
                                                        )
                 );
                    $redirectUrl = array('controller'=> 'Homes','action'=>'login');
                    $this->redirect($redirectUrl);
                }
                
            }
            $this->Flash->error('Password and confirm password do not match.', array(
                                                            'key' => 'positive'
                                                        )
                 );
            $this->redirect($this->referer());
            
        }
    }
    public function courtListing() {
        $this->layout = "frontend_dashboard";
        $this->loadModel('Country');
        $this->loadModel('Surface');
        $country = $this->Country->find('list',array('fields' => array('id','country_name')));
        $surface = $this->Surface->find('list',array('fields' => array('id','surface_name')));
        $this->loadModel('Court');    
        $conditions = array();    
        if (isset($this->request->query) && !empty($this->request->query)) {
            $searchData = array(
                'OR' => array(
                    'Court.court_name LIKE' => '%'. $this->request->query['search'] .'%',
                    'Court.court_number LIKE' => '%'. $this->request->query['search'] .'%',
                    'Court.court_type' => '%'. $this->request->query['search'] .'%'
                    )
                );
            $conditions = array_merge($conditions, $searchData);
        }
        $this->Paginator->settings = array(
                            'conditions' => $conditions,
                            'limit' => 10,
                            'contain' => array('CourtDay','Surface'),
                            'order' => 'Court.created desc'
                        );
        $courts = $this->Paginator->paginate('Court');
        $this->set(compact('country','surface','courts'));
    }
    public function addCourt() {
        $this->loadModel('Court');
        if ($this->request->is('post')) { 
            //pr($this->request->data);die;
            $this->request->data['Court']['user_id'] = $this->Auth->user('id');
            if ($this->Court->saveAssociated($this->request->data)) {
                $this->Flash->success('Court has been successfully added', array(
                                                            'key' => 'positive'
                                                        )
                 );
            } else {
                $this->Flash->error('The Court could not be created. Please, try again.', array(
                                                            'key' => 'positive'
                                                        )
                                );
            }
            return $this->redirect($this->referer());
        }
        
    }
}