<?php
App::uses('Component','Controller');
App::uses('CakeTime', 'Utility');
class QuickbookEntityComponent extends Component{


	public function create_item($data,$datas){
		$item = new IPPItem();
		$item->Name = preg_replace('/[^A-Za-z0-9\-]/', '', $datas[array_search('ITEM DESCRIPTION', $data)]);
        $item->UnitPrice = $datas[array_search('UNIT PRICE', $data)];  
        $item->IncomeAccountRef = 79;
        $item->ExpenseAccountRef = 80;
        $item->AssetAccountRef = 81;
        $item->TrackQtyOnHand = true;
        $item->QtyOnHand = 10;
        $item->Active = true;
        return $item;
	}

	public function create_line($data,$datas){
		$line = new IPPLine();
        $line->Description = preg_replace('/[^A-Za-z0-9\-]/', '', $datas[array_search('ITEM DESCRIPTION', $data)]);                   
        $line->Amount = $datas[array_search('EXTENDED AMT', $data)];
        $linedetailTypeEnum = new IPPLineDetailTypeEnum();
        $line->DetailType = "SalesItemLineDetail";
        $line->Qty = $datas[array_search('QTY', $data)];
        $line->SalesItemLineDetail = $this->create_item($data,$datas);
       
        return $line;
	}

	public function create_customer($data,$datas) {  
        $addrObj =  new IPPPhysicalAddress();
        $addrObj->Line1 = $datas[array_search('BILL ADR1', $data)];
        $addrObj->Line2 = $datas[array_search('BILL ADR2', $data)];
        $addrObj->City = $datas[array_search('BILL CITY', $data)];
        $addrObj->CountrySubDivisionCode = $datas[array_search('BILL ST', $data)];
        $addrObj->PostalCode = $datas[array_search('BILL ZIP', $data)];


        $customerObj = new IPPCustomer(); 
        // $customerObj->Id = 66;
        $customerObj->DisplayName = str_replace("'","`",substr($datas[array_search('BILL NAME', $data)], 0,24));
        $customerObj->GivenName = str_replace("'","`",substr($datas[array_search('BILL NAME', $data)], 0,24));
        $customerObj->BillAddr = $addrObj;
        return $customerObj;
    }

    public function create_invoice($data,$datas) {  
    	// pr($data);pr($datas);die;
        
        $invoice = new IPPInvoice();
        $invoice->DocNumber = $datas[array_search('DOC NBR', $data)]; 
        $invoice->TxnDate = date('Y-m-d',strtotime($datas[array_search('DOC DATE', $data)]));
       return $invoice;
    }
}