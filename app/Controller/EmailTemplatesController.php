<?php
class EmailTemplatesController extends AppController
{
	public $helper = array('Html', 'Form','Fck');
	public $components = array('Paginator','Flash');
	public $uses = array('EmailTemplate');
	public $layout = 'backend';	

	public function beforeFilter() {
		parent::beforeFilter();
	}

/**
 * Method admin_index to display all categories
 *
 * @return void 
 */
	public function admin_index() {
		$this->layout = "backend";
		$conditions = array('EmailTemplate.is_deleted' => 0);
			if (isset($this->request->query) && !empty($this->request->query)) {
			$searchData = array(
				'OR' => array(
					'EmailTemplate.template_used_for LIKE' => '%'. $this->request->query['search'] .'%',
					'EmailTemplate.subject LIKE' => '%'. $this->request->query['search'] .'%',
					)
				);
			$conditions = array_merge($conditions, $searchData);
		}
		$this->Paginator->settings = array(
							'conditions' => $conditions,
							'limit' => 10,
							'order' => 'EmailTemplate.id Asc'
						);
		$emailTemplates = $this->Paginator->paginate('EmailTemplate');
		$this->set('emailTemplates',$emailTemplates);
	}

/**
 * Method admin_editTemplate to save upadted records
 *
 * @return void 
 */
	public function admin_editTemplate($templateID=null) {
		if(isset($this->request->data) && !empty($this->request->data)) {
			if ($this->EmailTemplate->save($this->request->data)) {
				$this->Flash->success('Email template updated successfully. ', array(
                                                            'key' => 'positive'
                                                        )
                 );
				$this->redirect(array('action' => 'index', 'admin' => true));
			} else {
				$errors = $this->EmailTemplate->validationErrors;
	            if (!empty($errors)) {
	                $errorMsg = $this->_setValidaiotnError($errors);
	            }
				
				$this->Flash->error('Email template update request not completed. ', array(
                                                            'key' => 'positive'
                                                        )
                 );
			}
			$this->redirect($this->referer());
		}
		$this->layout = 'backend';
		$templateID = base64_decode($templateID);
		$template = $this->EmailTemplate->findById($templateID, array('id', 'template_used_for', 'subject' ,'mail_body'));
		$this->set('template',$template);
	}

	public function sendEmailNotification($to = null, $email_body = null, $subject = null, $bcc = null, $cc = null){
		App::uses('CakeEmail', 'Network/Email');
      	$email = new CakeEmail('smtp');
      	$email->emailFormat('both');
      	$email->from(Configure::read('Email.EmailAdmin'));
      	$email->sender(Configure::read('Email.EmailAdmin'));
      	$email->to($to);
      	$email->subject($subject);

      	if ($email->send($email_body)) {
			return true;
		}
		return false;
	}
}
