<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
		public $components = array(
        'Flash',
        'Auth' => array(
            'logoutRedirect' => array(
                'controller' => 'Homes',
                'action' => 'login',
            ),
            'loginAction' => array(
                'controller' => 'Homes',
                'action' => 'courtListing'
                ),
            'authenticate' => array(
                'Form' => array(
                    'passwordHasher' => 'Simple',
                    'fields' => array('username' => 'email')
                )
            ),
           // 'authorize' => array('Controller')
        ),
        'Cookie'
    );
	 public function beforeFilter() {
	 	// if(!empty($this->request->params['prefix']) && $this->request->params['prefix'] == 'admin'){
   //      $this->Auth->loginAction = array('controller' => 'Users','action' => 'login','admin'=>'true' );
   //    } else{
   //      $this->Auth->loginAction = array('controller' => 'Homes','action' => 'login');
   //    }
   //    $this->Auth->allow('signup');
	 }
	 public function _sendEmailMessage($to = null, $email_body = null, $subject = null, $bcc = null, $cc = null)
   	{     //pr($to);die;
        App::uses('CakeEmail', 'Network/Email');
         $email = new CakeEmail('smtp');
         $email->emailFormat('both');
         $email->from('support@bookacourt.com');
         $email->sender('support@bookacourt.com');
         $email->to($to);
         $email->subject($subject);          
         if ($email->send($email_body)) {
            return true;
        }
        return false;
   	}
	
}
