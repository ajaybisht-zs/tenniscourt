<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class CmsPagesController extends AppController {
	public $components = array('Flash','Paginator');
	public $layout = "backend";
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow("index");
    }

	/**
* Method admin_listCmsPages to view all the cms pages of the website
*
* @return void
*/
    public function admin_listCmsPages() {
    	$this->layout = "backend";
        $this->loadModel('CmsPage');    
        $conditions = array();    
        if (isset($this->request->query) && !empty($this->request->query)) {
            $searchData = array(
                'OR' => array(
                    'CmsPage.meta_title LIKE' => '%'. $this->request->query['search'] .'%',
                    'CmsPage.meta_keyword LIKE' => '%'. $this->request->query['search'] .'%',
                    'CmsPage.page_name LIKE' => '%'. $this->request->query['search'] .'%',
                    )
                );
            $conditions = array_merge($conditions, $searchData);
        }
        $this->Paginator->settings = array(
                            'conditions' => $conditions,
                            'limit' => 10,
                            'order' => 'CmsPage.id Asc'
                        );
        $emailTemplates = $this->Paginator->paginate('CmsPage');
        $this->set('getAllCmsPages',$emailTemplates);
    }/**
* Method admin_editCms to save upadted records
*
* @return void
*/
    public function admin_editCms($cmsID=null) {
    	$this->layout = "backend";
        $this->loadModel('CmsPage');
        if(isset($this->request->data) && !empty($this->request->data)) {
            if ($this->CmsPage->save($this->request->data)) {
                $this->Flash->success('Cms page updated successfully. ', array(
                                                            'key' => 'positive'
                                                        )
                 );
                $this->redirect(array('action' => 'listCmsPages', 'admin' => true));
            } else {
                $errors = $this->CmsPage->validationErrors;
                if (!empty($errors)) {
                    $errorMsg = $this->_setValidaiotnError($errors);
                }
                $this->Flash->error('Cms page update request not completed ', array(
                                                            'key' => 'positive'
                                                        )
                 );
            }
            $this->redirect($this->referer());
        }
        $cmsID = base64_decode($cmsID);
        $cms = $this->CmsPage->findById($cmsID, array(
            'id','meta_title', 'meta_description', 'meta_keyword', 'page_name', 'page_title'));
        //pr($cms);die;
        $this->set('cms',$cms);
    }
    /**
    * Method Index to Rendering Landing page of
    *
    * @return void
    */
        public function index($slug) { 
            $this->layout = 'contact';       
            $pageData = $this->CmsPage->find('first', array('conditions'=> array('CmsPage.meta_title'=>$slug)));
            $this->set(compact('pageData'));
    }
    public function admin_deactivate($id){
       $id = base64_decode($id);
       $this->layout = "backend";
       $this->CmsPage->id = $id;
       $userdata = $this->CmsPage->find('first', array('conditions' => array('CmsPage.id' => $id)));
       if($userdata['CmsPage']['is_activated'] == 1){
            $this->CmsPage->saveField('is_activated', 0);           
       }
       elseif ($userdata['CmsPage']['is_activated'] == 0) {
            $this->CmsPage->saveField('is_activated', 1);  
        } 
       $this->Flash->success('CmsPages Status has been changed successfully', array(
                                                            'key' => 'positive'
                                                        )
                                );
       $this->redirect(array('controller' => 'CmsPages', 'action' => 'listCmsPages', 'admin' => true));
    }
   
}